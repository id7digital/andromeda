﻿using System.ServiceModel;
using ID7Digital.EventBook.Domain.Models;
using System.Collections.Generic;

namespace ID7Digital.EventBook.Services.Abstract
{
    [ServiceContract]
    public interface IStatusUpdateService
    {
        [OperationContract]
        List<StatusUpdate> GetTopStatusUpdatesByAccountId(int accountId, int number);
    }
}
