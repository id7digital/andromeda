﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ID7Digital.EventBook.Domain.Models;

namespace ID7Digital.EventBook.Services.Abstract
{
    public interface IFriendInviteService
    {
        FriendInvitation GetFriendInvitationByGuid(Guid? friendInvitationGuid);
        void CleanUpFriendInvitationsForEmail(FriendInvitation friendInvitation);
        bool CreateFriendRequest(int friendId, int usersId);
    }
}
