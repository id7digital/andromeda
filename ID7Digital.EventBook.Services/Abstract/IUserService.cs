﻿
using System.Collections.Generic;
using System.ServiceModel;
using System.Web.Security;

namespace ID7Digital.EventBook.Services.Abstract
{
    [ServiceContract]
    public interface IUserService
    {
        [OperationContract]
        MembershipUser CreateUser(string userName, string password, string email = null);
        [OperationContract]
        void DeleteUser(string userName);
        [OperationContract]
        IEnumerable<string> GetUsers();
        [OperationContract]
        IEnumerable<string> GetUsers(string filter);
        [OperationContract]
        void SetPassword(string userName, string password);
        [OperationContract]
        void SetRolesForUser(string userName, IEnumerable<string> roles);
        [OperationContract]
        IEnumerable<string> GetRolesForUser(string userName);
        [OperationContract]
        bool ResetPasswordWithAnswer(string userName, string password, string answer);
        [OperationContract]
        IEnumerable<string> GetRoles();
        [OperationContract]
        void CreateRole(string roleName);
        [OperationContract]
        void DeleteRole(string roleName);
        [OperationContract]
        MembershipUser GetExisitingUser(string userName);
        [OperationContract]
        bool ExistingUser(string userName);
        [OperationContract]
        bool ExistingEmail(string email);

    }
}
