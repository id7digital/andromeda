﻿using System.ServiceModel;
using ID7Digital.EventBook.Domain.Models;
using System.Collections.Generic;

namespace ID7Digital.EventBook.Services.Abstract
{
    [ServiceContract]
    public interface IFriendService
    {
        [OperationContract]
        List<Friend> GetFriendsByAccountId(int accountId);
        [OperationContract]
        List<Account> GetFriendsAccountsByAccountId(int accountId);
        [OperationContract]
        bool IsFriend(Account account, Account accountBeingViewed);
    }
}
