﻿using System.Collections.Generic;
using System.ServiceModel;
using ID7Digital.EventBook.Domain.Models;
using ID7Digital.EventBook.Services.ViewModels;


namespace ID7Digital.EventBook.Services.Abstract
{
    [ServiceContract]
    public interface IAccountService
    {
        [OperationContract]
        Account GetAccountById(int id);
        [OperationContract]
        void RemoveAccount(Account account);
        [OperationContract]
        bool CreateAccount(SignUpModel signUpModel);
        [OperationContract]
        Account FindAccountByActivationGuid(string activationGuid);
        [OperationContract]
        Account FindAccountByUserName(string activationGuid);
        [OperationContract]
        Account FindAccountByPasswordResetCode(string passwordResetCode);
        [OperationContract]
        List<FriendViewModel> SearchAccounts(string searchText);
        [OperationContract]
        bool StartEmailResetPasswordProcess(string userName, string emailAddress, out string passwordResetCode);
        [OperationContract]
        bool StartEmailResetPasswordProcess(string userName, string emailAddress);
        [OperationContract]
        bool StartEmailResetPasswordProcess(int id);
        [OperationContract]
        ActivationViewModel ActivateAccount(Account account);
        [OperationContract]
        void SaveChanges();
        [OperationContract]
        bool UpdateAccount(Account account);
        [OperationContract]
        bool TryUnlockAfterWaitingPeriod(string accountName, int maxLockOutTimeInSecs);
        [OperationContract]
        void UpdateLoginAttemptTracking(string accountName, bool validated);
    }
}
