﻿using System.Collections.Generic;
using ID7Digital.EventBook.Domain.Abstract;
using ID7Digital.EventBook.Domain.Concrete;
using ID7Digital.EventBook.Services.Abstract;
using Ninject;

namespace ID7Digital.EventBook.Services.Concrete
{
    public class StatusUpdateService : IStatusUpdateService
    {
        protected IStatusUpdateRepository _StatusUpdateRepository;

        public StatusUpdateService()
        {
            var kernel = new StandardKernel();
            kernel.Bind<IStatusUpdateRepository>().To<StatusUpdateRepository>();
            _StatusUpdateRepository = kernel.Get<IStatusUpdateRepository>();
        }

        public List<Domain.Models.StatusUpdate> GetTopStatusUpdatesByAccountId(int accountId, int number)
        {
            return _StatusUpdateRepository.GetTopStatusUpdatesByAccountId(accountId, number);
        }
    }
}
