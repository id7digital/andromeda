﻿using System.Collections.Generic;
using ID7Digital.EventBook.Domain.Abstract;
using ID7Digital.EventBook.Domain.Concrete;
using ID7Digital.EventBook.Domain.Helpers;
using ID7Digital.EventBook.Domain.Models;
using ID7Digital.EventBook.EmailProvider;
using ID7Digital.EventBook.Services.Abstract;
using ID7Digital.EventBook.Services.ViewModels;
using Ninject;
using System.Linq;

namespace ID7Digital.EventBook.Services.Concrete
{
    public class AccountService : IAccountService
    {
        protected IAccountRepository _AccountRepository;

        public AccountService()
        {
            var kernel = new StandardKernel();
            kernel.Bind<IAccountRepository>().To<AccountRepository>();
            _AccountRepository = kernel.Get<IAccountRepository>();
        }

        public bool CreateAccount(SignUpModel signUpModel)
        {
                // implicit type conversion used here to cast an accountviewmodel type to an account type.
                Account accountToBeCreated = signUpModel;
                signUpModel.ActivationCode = accountToBeCreated.ActivationCode;

                var hasAccountBeenCreated = _AccountRepository.CreateAccount(accountToBeCreated);

                return hasAccountBeenCreated;       
        }

        public ActivationViewModel ActivateAccount(Account account)
        {
            var viewModel = new ActivationViewModel { ActivationCompleted = false, SignIn = new SignInModel() };

            if (!account.IsActivated)
            {
                account.IsActivated = true;
                SaveChanges();

                viewModel.ActivationCompleted = true;

                return viewModel;
            }

            return viewModel;
        }

        public Account GetAccountById(int id)
        {            
            return _AccountRepository.FindAccountById(id);
        }

        public Account FindAccountByPasswordResetCode(string passwordResetCode)
        {
            return _AccountRepository.FindAccountByPasswordResetCode(passwordResetCode);
        }

        public Account FindAccountByActivationGuid(string activationGuid)
        {
            return _AccountRepository.FindAccountByActivationGuid(activationGuid);
        }

        public Account FindAccountByUserName(string activationGuid)
        {
            return _AccountRepository.FindAccountByUserName(activationGuid);
        }
        public void RemoveAccount(Account account)
        {
            _AccountRepository.RemoveAccount(account);
        }

        public bool UpdateAccount(Account account)
        {
            return _AccountRepository.UpdateAccount(account);
        }

        public bool TryUnlockAfterWaitingPeriod(string accountName, int maxLockOutTimeInSecs)
        {
           return !UserManagementHelper.TryUnlockAccountAfterWaitingPeriod(accountName, _AccountRepository, maxLockOutTimeInSecs);         
        }

        public void UpdateLoginAttemptTracking(string accountName, bool validated)
        {
            UserManagementHelper.UpdateLoginAttemptTracking(accountName, validated, _AccountRepository);
        }

        public bool StartEmailResetPasswordProcess(string userName, string emailAddress, out string passwordResetCode)
        {
            return _AccountRepository.StartEmailResetPasswordProcess(userName, emailAddress, out passwordResetCode);
        }

        public bool StartEmailResetPasswordProcess(string userName, string emailAddress)
        {
            return _AccountRepository.StartEmailResetPasswordProcess(userName, emailAddress);
        }

        public bool StartEmailResetPasswordProcess(int id)
        {
            return _AccountRepository.StartEmailResetPasswordProcess(id);
        }

        public void SaveChanges()
        {
            _AccountRepository.SaveChanges();
        }

        #region Extended Services
        public static void SendActivationPartialToEmail(ActivationEmailModel model, string subject, string view)
        {
            Email.SendEmail(model.Email, subject, view);
        }
     
        #endregion

        public List<FriendViewModel> SearchAccounts(string searchText)
        {
           return _AccountRepository.SearchAccounts(searchText).Select(account => new FriendViewModel { FirstName = account.FirstName, UserName = account.UserName }).ToList();     
        }

    }
}
