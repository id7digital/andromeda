﻿using ID7Digital.EventBook.Domain.Abstract;
using ID7Digital.EventBook.Domain.Concrete;
using ID7Digital.EventBook.Services.Abstract;
using Ninject;
using System;
using System.Collections.Generic;
using System.Web.Security;

namespace ID7Digital.EventBook.Services.Concrete
{
    public class UserService : IUserService
    {
        protected IUserManagementRepository userManagemtnyRepo;

        public UserService()
        {
            var kernel = new StandardKernel();
            kernel.Bind<IUserManagementRepository>().To<UserManagementRepository>();
            userManagemtnyRepo = kernel.Get<IUserManagementRepository>();
        }

        public MembershipUser CreateUser(string userName, string password, string email = null)
        {
           var user = userManagemtnyRepo.CreateUser(userName, password, email);

            return user;
        }

        public void DeleteUser(string userName)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> GetUsers()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> GetUsers(string filter)
        {
            throw new NotImplementedException();
        }

        public void SetPassword(string userName, string password)
        {
            throw new NotImplementedException();
        }

        public void SetRolesForUser(string userName, IEnumerable<string> roles)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> GetRolesForUser(string userName)
        {
            throw new NotImplementedException();
        }

        public bool ResetPasswordWithAnswer(string userName, string password, string answer)
        {
           return userManagemtnyRepo.ResetPasswordWithAnswer(userName, password, answer);
        }

        public IEnumerable<string> GetRoles()
        {
            throw new NotImplementedException();
        }

        public void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public void DeleteRole(string roleName)
        {
            throw new NotImplementedException();
        }


        public MembershipUser GetExisitingUser(string userName)
        {
            return userManagemtnyRepo.GetExisitingUser(userName);
        }


        public bool ExistingUser(string userName)
        {
            return userManagemtnyRepo.ExistingUser(userName);
        }

        public bool ExistingEmail(string email)
        {
            return userManagemtnyRepo.ExistingUser(email);
        }
    }
}
