﻿using ID7Digital.EventBook.Domain.Abstract;
using ID7Digital.EventBook.Domain.Concrete;
using ID7Digital.EventBook.Domain.Models;
using ID7Digital.EventBook.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;

namespace ID7Digital.EventBook.Services.Concrete
{
    public class FriendInviteService : IFriendInviteService
    {
        protected IFriendInvitationRepository _FriendInvitationRepository;

        public FriendInviteService()
        {
             var kernel = new StandardKernel();
            kernel.Bind<IFriendInvitationRepository>().To<FriendInvitationRepository>();
            _FriendInvitationRepository = kernel.Get<IFriendInvitationRepository>();
        }

        public FriendInvitation GetFriendInvitationByGuid(Guid? friendInvitationGuid)
        {
            return _FriendInvitationRepository.GetFriendInvitationByGuid(friendInvitationGuid);
        }

        public void CleanUpFriendInvitationsForEmail(FriendInvitation friendInvitation)
        {
            _FriendInvitationRepository.CleanUpFriendInvitationsForEmail(friendInvitation);
        }

        public void SaveChanges()
        {
            _FriendInvitationRepository.SaveChanges();
        }

        public bool CreateFriendRequest(int friendId, int userId)
        {
            return _FriendInvitationRepository.CreateFriendRequest(friendId, userId);
        }
    }
}
