﻿using System;
using System.Collections.Generic;
using System.Linq;
using ID7Digital.EventBook.Domain.Abstract;
using ID7Digital.EventBook.Domain.Concrete;
using ID7Digital.EventBook.Domain.Models;
using ID7Digital.EventBook.Services.Abstract;
using Ninject;

namespace ID7Digital.EventBook.Services.Concrete
{
    public class FriendService : IFriendService
    {

        protected IFriendRepository _FriendRepository;
        protected IFriendInvitationRepository _FriendInvitationRepository;
        protected IAccountRepository _AccountRepository;

        public FriendService()
        {
            var kernel = new StandardKernel();
            kernel.Bind<IFriendRepository>().To<FriendRepository>();
            _FriendRepository = kernel.Get<IFriendRepository>();

            kernel.Bind<IFriendInvitationRepository>().To<FriendInvitationRepository>();
            _FriendInvitationRepository = kernel.Get<IFriendInvitationRepository>();


            kernel.Bind<IAccountRepository>().To<AccountRepository>();
            _AccountRepository = kernel.Get<IAccountRepository>();
        }

        public List<Friend> GetFriendsByAccountId(int accountId)
        {
            return _FriendRepository.GetFriendsByAccountId(accountId);
        }


        public bool IsFriend(Account account, Account accountBeingViewed)
        {
            if (account == null)
                return false;
            if (accountBeingViewed == null)
                return false;
            if (account.ID == accountBeingViewed.ID)
                return true;

            var friend =_FriendRepository.GetFriendsByAccountId(accountBeingViewed.ID).FirstOrDefault(f => f.MyFriendsAccountId == account.ID);
            if (friend != null)
                return true;

            return false;
        }

        public bool CreateFriendFromFriendInvitation(Guid InvitationKey, Account InvitationTo, bool ExistingMember)
        {
            //update friend invitation request
            var friendInvitation = _FriendInvitationRepository.GetFriendInvitationByGuid(InvitationKey);
           
            //validate if the friend request is for the same person who has logged in
            //however if this is a new member (new registration), then don't validate as the
            //person may use a different email during registration

            if (ExistingMember && (friendInvitation.Email != InvitationTo.EmailAddresses[0].Email))
                return false;

            friendInvitation.BecameAccountId = InvitationTo.ID;

            _FriendInvitationRepository.SaveChanges();
            
            _FriendInvitationRepository.CleanUpFriendInvitationsForEmail(friendInvitation);

          //create friendship
           var friend = new Friend {AccountId = friendInvitation.AccountId, MyFriendsAccountId = InvitationTo.ID};

           _FriendRepository.SaveChanges();

            var InvitationFrom = _AccountRepository.FindAccountById(friendInvitation.AccountId);

           return true;
        }


        public List<Account> GetFriendsAccountsByAccountId(int accountId)
        {
            return _FriendRepository.GetFriendsAccountsByAccountId(accountId);
        }
    }
}
