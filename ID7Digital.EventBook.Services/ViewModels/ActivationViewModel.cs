﻿
namespace ID7Digital.EventBook.Services.ViewModels
{
    public class ActivationViewModel
    {
        public bool ActivationCompleted { get; set; }

        public SignInModel SignIn { get; set; }
    }
}
