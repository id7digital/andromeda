﻿
namespace ID7Digital.EventBook.Services.ViewModels
{
    public class FriendViewModel
    {
        public string UserName { get; set; }

        public string FirstName { get; set; }
    }
}
