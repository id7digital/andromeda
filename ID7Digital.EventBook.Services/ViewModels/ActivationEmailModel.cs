﻿using System.Configuration;
using ID7Digital.EventBook.Domain.Models;

namespace ID7Digital.EventBook.Services.ViewModels
{
    public class ActivationEmailModel
    {
        public string ActivationCode { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string ResetPasswordCode { get; set; }
        public string FirstName { get; set; }

        public string LoginURL
        {
            get
            {
                return ConfigurationManager.AppSettings["EventBookURL"];
            }
        }

        public string ActivationLink
        {
            get
            {
                return ConfigurationManager.AppSettings["EventBookURL"] + "Activate?activationCode=" + ActivationCode;
            }
        }

        public string ResetPasswordLink
        {
            get
            {
                return ConfigurationManager.AppSettings["EventBookURL"] + "ResetPasswordFromEmail?resetpasswordcode=" + ResetPasswordCode;
            }
        }

        public string ReminderLoginLink
        {
            get
            {
                return ConfigurationManager.AppSettings["EventBookURL"];
            }
        }

        public ActivationEmailModel(SignUpModel signUpModel)
        {
            ActivationCode = signUpModel.ActivationCode;
            Email = signUpModel.Email;
            UserName = signUpModel.UserName;
            FirstName = signUpModel.FirstName;
        }

        public ActivationEmailModel(Account account)
        {
            ActivationCode = account.ActivationCode;
            Email = account.GetPrimaryEmailAddress().Email;
            UserName = account.UserName;
            FirstName = account.FirstName;
        }

        public ActivationEmailModel()
        {
        }

    }
}
