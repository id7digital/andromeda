﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using ID7Digital.EventBook.Domain.Enum;
using ID7Digital.EventBook.Domain.Models;

namespace ID7Digital.EventBook.Services.ViewModels
{
    [PropertiesMustMatch("Password", "ConfirmPassword", ErrorMessage = "The password and confirmation password do not match.")]
    public class SignUpModel
    {
        public GenderEnum Gender { get; set; }

        [Required]
        [StringLength(200)]
        [DisplayName("First Name")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(200)]
        [DisplayName("Last Name")]
        public string LastName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [RegularExpression("^[a-zA-Z0-9_\\+-]+(\\.[a-zA-Z0-9_\\+-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*\\.([a-zA-Z]{2,4})$", ErrorMessage = "Invalid email")]
        [Display(Name = "E-mail")]
        public string Email { get; set;}

        [Required]
        [Display(Name = "User name")]
        [RegularExpression(@"^[a-zA-Z0-9]+([a-zA-Z0-9\\._\\+-@&])*[a-zA-Z0-9]$", ErrorMessage = "User Name contains Invalid Characters")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [DisplayName("Password")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [DisplayName("Confirm password")]
        public string ConfirmPassword { get; set; }

        public string ActivationCode { get; set; }

        [MustBeTrue(ErrorMessage = "You must accept the terms and conditions")]
        [DisplayName("Accept Terms And Conditions")]
        public bool TermsAndConditions { get; set; }

        public bool ShowValidation { get; set; }
      
     
        /// <summary>
        /// Validation attribute that demands that a boolean value must be true.
        /// </summary>
        [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
        public class MustBeTrueAttribute : ValidationAttribute
        {
            public override bool IsValid(object value)
            {
                return value != null && value is bool && (bool)value;
            }
        }

        // Maps back to the account
        public static implicit operator Account(SignUpModel signUpModel)
        {
           return new Account()
                {
                UserGuid = Guid.NewGuid().ToString(),
                ActivationCode = Guid.NewGuid().ToString(),
                UserName = signUpModel.UserName,
                Gender = signUpModel.Gender,
                FirstName = signUpModel.FirstName,
                LastName = signUpModel.LastName,
                EmailAddresses = new List<EmailAddress>{new EmailAddress{Description = "Default Email Address", Email = signUpModel.Email, EmailType = EmailTypeEnum.Home, IsPrimary = true}},
                DateRegistered = DateTime.Now,
                AuthenticationEnabled = false,
                AuthenticateByEmail = false,
                AuthenticateBySMS = false,
                AuthenticateByPreGeneratedCode = false,
                TwoFactorIpAddressesEnabled = false,
                AuthenticationCode = null,
                TwoFactorAttempts = 0,
                FailedAttemptsCount = 0,
                LastUpdated = DateTime.Now
                };

        }
    }

    [PropertiesMustMatchAttribute("NewPassword", "ConfirmPassword", ErrorMessage = "The new password and confirmation password do not match.")]
    public class ResetPasswordModel
    {
        public string UserName { get; set; }

        [DisplayName("Secret Question")]
        public string SecretQuestion { get; set; }

        [Required]
        [DisplayName("Secret Question Answer")]
        public string SecretQuestionAnswer { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [DisplayName("New Password")]
        public string NewPassword { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [DisplayName("Confirm password")]
        public string ConfirmPassword { get; set; }
    }

    public sealed class PropertiesMustMatchAttribute : ValidationAttribute
    {
        private const string _defaultErrorMessage = "'{0}' and '{1}' do not match.";
        private readonly object _typeId = new object();

        public PropertiesMustMatchAttribute(string originalProperty, string confirmProperty)
            : base(_defaultErrorMessage)
        {
            OriginalProperty = originalProperty;
            ConfirmProperty = confirmProperty;
        }

        public string ConfirmProperty { get; private set; }
        public string OriginalProperty { get; private set; }

        public override object TypeId
        {
            get
            {
                return _typeId;
            }
        }

        public override string FormatErrorMessage(string name)
        {
            return String.Format(CultureInfo.CurrentUICulture, ErrorMessageString,
                OriginalProperty, ConfirmProperty);
        }

        public override bool IsValid(object value)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(value);
            object originalValue = properties.Find(OriginalProperty, true /* ignoreCase */).GetValue(value);
            object confirmValue = properties.Find(ConfirmProperty, true /* ignoreCase */).GetValue(value);
            return Object.Equals(originalValue, confirmValue);
        }
    }
}
