﻿using System.Web;
using System.Web.Mvc;

namespace ID7Digital.EB.Static
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}