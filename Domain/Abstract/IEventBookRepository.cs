﻿
using System.Collections.Generic;
using System.Linq;

namespace ID7Digital.EventBook.Domain.Abstract
{
    public interface IEventBookRepository<T>
    {
        T Add(T entity);
        T Attach(T obj);
        T Create();
        IQueryable<T> Fetch();
        IEnumerable<T> GetAll();
        T GetByID(int ID);
        T Remove(T entity);
        void SaveChanges();
    }
}
