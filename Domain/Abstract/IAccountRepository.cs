﻿using System.Collections.Generic;
using System.Linq;
using ID7Digital.EventBook.Domain.Models;

namespace ID7Digital.EventBook.Domain.Abstract
{
   public interface IAccountRepository : ICommon
   {
       IQueryable<Account> FindAllAccounts();
       Account FindAccountById(int id);   
       Account FindAccountByUserGuid(string userGuid);
       Account FindAccountByUserName(string userName);
       Account FindAccountByActivationGuid(string activationGuid);
       Account FindAccountByEmailAddress(string emailAddress);
       Account FindAccountByPrimaryEmailAddress(string emailAddress);
       Account FindAccountByPasswordResetCode(string passwordResetCode);
       List<Account> SearchAccounts(string searchText);
       Account GetCurrentAccount();
       bool StartEmailResetPasswordProcess(string userName, string emailAddress, out string passwordResetCode);
       bool StartEmailResetPasswordProcess(string userName, string emailAddress);
       bool StartEmailResetPasswordProcess(int id); 
       bool CreateAccount(Account account);
       bool UpdateAccount(Account account);
       void RemoveAccount(Account account);
       void SaveChanges();
   }
}
