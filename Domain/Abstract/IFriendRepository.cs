﻿using ID7Digital.EventBook.Domain.Models;
using System.Collections.Generic;

namespace ID7Digital.EventBook.Domain.Abstract
{
    public interface IFriendRepository : ICommon
    {
        List<Friend> GetFriendsByAccountId(int accountId);
        List<Account> GetFriendsAccountsByAccountId(int accountId);
        void SaveChanges();
      
    }
}
