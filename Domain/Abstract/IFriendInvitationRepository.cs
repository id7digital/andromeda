﻿using ID7Digital.EventBook.Domain.Models;
using System;

namespace ID7Digital.EventBook.Domain.Abstract
{
    public interface IFriendInvitationRepository : ICommon
    {
        FriendInvitation GetFriendInvitationByGuid(Guid? friendInvitationGuid);
        void CleanUpFriendInvitationsForEmail(FriendInvitation friendInvitation);
        bool CreateFriendRequest(int friendId, int usersId);
        void SaveChanges();
    }
}
