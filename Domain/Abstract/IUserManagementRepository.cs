﻿using System.Collections.Generic;
using System.Web.Security;


namespace ID7Digital.EventBook.Domain.Abstract
{
    public interface IUserManagementRepository
    {
        MembershipUser CreateUser(string userName, string password, string email = null);
        void DeleteUser(string userName);
        IEnumerable<string> GetUsers();
        IEnumerable<string> GetUsers(string filter);
        void SetPassword(string userName, string password);
        void SetRolesForUser(string userName, IEnumerable<string> roles);
        IEnumerable<string> GetRolesForUser(string userName);
        bool ResetPasswordWithAnswer(string userName, string password, string answer);
        IEnumerable<string> GetRoles();
        void CreateRole(string roleName);
        void DeleteRole(string roleName);
        MembershipUser GetExisitingUser(string userName);
        bool ExistingUser(string userName);
        bool ExistingEmail(string email);
    }
}
