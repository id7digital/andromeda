﻿using ID7Digital.EventBook.Domain.Models;
using System.Collections.Generic;

namespace ID7Digital.EventBook.Domain.Abstract
{
    public interface IStatusUpdateRepository : ICommon
    {
        List<StatusUpdate> GetTopStatusUpdatesByAccountId(int accountId, int number);
    }
}
