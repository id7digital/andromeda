﻿using ID7Digital.EventBook.Domain.Enum;
using System;
using System.ComponentModel.DataAnnotations;

namespace ID7Digital.EventBook.Domain.Models
{
    public class EmailAddress : IEBEntity
    {
        public EmailAddress()
        {
        }

        public EmailAddress(EmailAddress emailAddress)
        {
            Email = emailAddress.Email;
            Description = emailAddress.Description;
            IsPrimary = emailAddress.IsPrimary;
            EmailType = emailAddress.EmailType;

        }

        [Key]
        public int ID { get; set; }

        [Required(ErrorMessage = "Email address cannot be blank")]
        [RegularExpression(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$|^$", ErrorMessage = "Email address format is incorrect")]
        [StringLength(200)]
        public string Email
        {
            get;
            set;
        }

        [StringLength(200)]
        public string Description
        {
            get;
            set;
        }

        public bool IsPrimary { get; set; }

        public EmailTypeEnum EmailType { get; set; }

        public DateTime? SynchronisationModifiedDate { get; set; }

        public string SyncronisationMapping { get; set; }
    }

}
