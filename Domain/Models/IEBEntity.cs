﻿using System;

namespace ID7Digital.EventBook.Domain.Models
{
    public interface IEBEntity
    {
        int ID { get; set; }
        DateTime? SynchronisationModifiedDate { get; set; }
        string SyncronisationMapping { get; set; }
        
    }
}
