﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ID7Digital.EventBook.Domain.Models
{
    public class Address : IEBEntity
    {

        [Key]
        public int ID { get; set; }

        public string Description { get; set; }

        [StringLength(40)]
        public string BuildingName { get; set; }

        public string BuildingNumber { get; set; }

        [StringLength(40)]
        public string ThoroughfareOrStreet { get; set; }

        [StringLength(40)]
        public string Locality { get; set; }

        [StringLength(40)]
        public string Town { get; set; }

        [StringLength(8)]
        public string PostalCode { get; set; }

        [StringLength(40)]
        public string County { get; set; }

        public virtual Country Country { get; set; }

        public bool IsPrimaryAddress { get; set; }

        public virtual Account UserAccount { get; set; }

        public DateTime? SynchronisationModifiedDate { get; set; }

        public string SyncronisationMapping { get; set; }
    }
}
