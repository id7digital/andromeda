﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ID7Digital.EventBook.Domain.Models
{
    public class Country : IEBEntity
    {
        [Key]
        public int ID { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        [StringLength(10)]
        public string CountryCode { get; set; }

        public DateTime? SynchronisationModifiedDate { get; set; }

        public string SyncronisationMapping { get; set; }
    }
}
