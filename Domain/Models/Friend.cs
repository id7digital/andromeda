﻿using System;
using System.ComponentModel.DataAnnotations;
namespace ID7Digital.EventBook.Domain.Models
{
    public class Friend : IEBEntity
    {
        [Key]
        public int ID { get; set; }

        public int AccountId { get; set; }

        public int MyFriendsAccountId { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? SynchronisationModifiedDate { get; set; }

        public string SyncronisationMapping { get; set; }
    }
}
