﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ID7Digital.EventBook.Domain.Models
{
    public class StatusUpdate : IEBEntity
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public int AccountId { get; set; }

        [Required]
        [StringLength(500)]
        public string Status { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? SynchronisationModifiedDate { get; set; }

        public string SyncronisationMapping { get; set; }
    }
}
