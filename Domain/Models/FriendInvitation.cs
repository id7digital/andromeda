﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ID7Digital.EventBook.Domain.Models
{
    public class FriendInvitation : IEBEntity
    {
        [Key]
        public int ID { get; set; }

        public int AccountId { get; set; }

        public string Email { get; set; }

        public Guid? FriendInvitationGuid { get; set; }

        public DateTime? CreateDate { get; set; }

        public int BecameAccountId { get; set; }

        public DateTime? SynchronisationModifiedDate { get; set; }

        public string SyncronisationMapping { get; set; }
    }
}
