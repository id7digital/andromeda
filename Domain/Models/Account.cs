﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using ID7Digital.EventBook.Domain.Enum;

namespace ID7Digital.EventBook.Domain.Models
{
   public  class Account : IEBEntity
    {
        public Account()
        {
            DateRegistered = DateTime.Now;
            AuthenticationEnabled = false;
            AuthenticateByEmail = false;
            AuthenticateBySMS = false;
            AuthenticateByPreGeneratedCode = false;
            TwoFactorIpAddressesEnabled = false;
            AuthenticationCode = null;
            TwoFactorAttempts = 0;
            TwoFactorIpAddressesEnabled = false;
            FailedAttemptsCount = 0;
            IsLocked = false;
            LastUpdated = DateTime.Now;
        }

        [Key]
        public int ID { get; set; }

        [Required]
        [StringLength(38)]
        public string UserGuid { get; set; }

        [StringLength(50)]
        public string Title { get; set; }

        [StringLength(200)]
        public string FirstName { get; set; }

        [StringLength(200)]
        public string LastName { get; set; }

        [StringLength(200)]
        public string MiddleName { get; set; }

        private string _displayName;

        [StringLength(200)]
        public string DisplayName
        {
            get { return _displayName ?? FullName; }

            set
            {
                _displayName = value;
            }
        }

        [StringLength(200)]
        public String FullName
        {
            get { return (FirstName != null && LastName != null ? string.Format("{0} {1}", FirstName, LastName) : "N/A"); }
        }

        [StringLength(200)]
        public string JobTitle { get; set; }

        public GenderEnum Gender { get; set; }

        [StringLength(38)]
        public string ActivationCode { get; set; }

        [StringLength(1000)]
        public string ActivationReturnUrl { get; set; }

        public bool IsActivated { get; set; }

        public int FailedAttemptsCount { get; set; }

        public bool IsLocked { get; private set; }

        public DateTime? LastLockOutDate { get; private set; }

        [StringLength(200)]
        public string AvatarFileName { get; set; }

        [StringLength(50)]
        public string AvatarContentType { get; set; }


        public DateTime DateRegistered { get; set; }

        public DateTime LastUpdated { get; set; }

        [StringLength(38)]
        public string PasswordResetCode { get; set; }

        [NotMapped]
        public bool IsAvatarOverriden
        {
            get { return !string.IsNullOrEmpty(AvatarFileName); }
        }


        /// <summary>
        /// Used to be able to look up users by name without contacting the membership provider as usernames can not change
        /// </summary>
        [StringLength(200)]
        [DisplayName("Username")]
        public string UserName { get; set; }

        private List<Address> _addresses;
        public virtual List<Address> Addresses
        {
            get
            {
                return _addresses ?? (_addresses = new List<Address>());
            }
            set
            {
                _addresses = value;
            }
        }

        private List<EmailAddress> _emailaddresses;
        public virtual List<EmailAddress> EmailAddresses
        {
            get
            {
                return _emailaddresses ?? (_emailaddresses = new List<EmailAddress>());
            }
            set
            {
                _emailaddresses = value;
            }
        }

        public bool AuthenticationEnabled { get; set; }

        public string AuthenticationCode { get; set; }

        public int TwoFactorAttempts { get; set; }

        public bool AuthenticateByEmail { get; set; }

        public bool AuthenticateBySMS { get; set; }

        public bool AuthenticateByPreGeneratedCode { get; set; }

        [NotMapped]
        public int AuthenticationMethodsUsedCount
        {
            get
            {
                var count = 0;

                if (AuthenticateByEmail) count++;
                if (AuthenticateBySMS) count++;
                if (AuthenticateByPreGeneratedCode) count++;

                return count;
            }
        }

        public bool TwoFactorIpAddressesEnabled { get; set; }

        public void LockAccount()
        {
            IsLocked = true;
            LastLockOutDate = DateTime.Now;
        }

        public void UnlockAccount()
        {
            IsLocked = false;
            FailedAttemptsCount = 0;
        }

        public EmailAddress GetPrimaryEmailAddress()
        {
            return EmailAddresses.FirstOrDefault(x => x.IsPrimary);
        }

        public PasswordStrengthEnum PasswordStrength { get; set; }

        public DateTime? SynchronisationModifiedDate { get; set; }

        public string SyncronisationMapping { get; set; }


    }
}
