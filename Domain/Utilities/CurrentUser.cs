﻿using ID7Digital.EventBook.Domain.Models;
using System.Web;

namespace ID7Digital.EventBook.Domain.Utilities
{
    public static class CurrentUser
    {
        public static LoggedInUser UserInformation
        {
            get
            {
                if (HttpContext.Current.Session["UserInformation"] == null)
                {
                    HttpContext.Current.Session["UserInformation"] = new LoggedInUser();
                }

                return (LoggedInUser)HttpContext.Current.Session["UserInformation"];
            }
            set
            {
                HttpContext.Current.Session["UserInformation"] = value;
            }
        }

        public static bool IsLoggedIn
        {
            get
            {
                return HttpContext.Current.Session["UserInformation"] != null && UserInformation.AccountId > 0;
            }
        }

        public static void ClearSessionData()
        {
            UserInformation = null;
        }
    }

    public class LoggedInUser
    {
        public LoggedInUser(Account account)
        {
            UserUniqueId = account.UserGuid;
            UserName = account.UserName;
            AccountId = account.ID;
            IsActivated = account.IsActivated;
        }

        public LoggedInUser()
        {
        }

        public string UserUniqueId { get; set; }
        public string UserName { get; set; }
        public int AccountId { get; set; }
        public bool IsActivated { get; set; }

    }
}
