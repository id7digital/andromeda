﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;


namespace ID7Digital.EventBook.Domain.Helpers
{
   public static class ImageHelper
    {
       public static Image ResizeImageWithAspectRatio(Image img, int width, int height)
       {
           int sourceWidth = img.Width;
           int sourceHeight = img.Height;
           int destX = 0;
           int destY = 0;

           // Return original image if it fits within the specified resize dimensions
           // i.e. don't scale it up so we preserve image quality
           if (img.Width <= width && img.Height <= height)
           {
               return img;
           }

           float nPercent;

           float nPercentW = width / (float)sourceWidth;
           float nPercentH = height / (float)sourceHeight;
           if (nPercentH < nPercentW)
           {
               nPercent = nPercentH;
               destX = System.Convert.ToInt16((width - (sourceWidth * nPercent)) / 2);
           }
           else
           {
               nPercent = nPercentW;
               destY = System.Convert.ToInt16((height - (sourceHeight * nPercent)) / 2);
           }

           var destWidth = (int)(sourceWidth * nPercent);
           var destHeight = (int)(sourceHeight * nPercent);

           var bitmap = new Bitmap(width, height, PixelFormat.Format32bppArgb);
           bitmap.SetResolution(img.HorizontalResolution, img.VerticalResolution);

           Graphics graphics = Graphics.FromImage(bitmap);
           //graphics.Clear(Color.Transparent);
           graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;

           graphics.DrawImage(img,
               new Rectangle(destX, destY, destWidth, destHeight),
               new Rectangle(0, 0, sourceWidth, sourceHeight),
               GraphicsUnit.Pixel);

           graphics.Dispose();
           return bitmap;
       }
       /// <summary>
       /// Converts and resizes an image file to a bitmap
       /// </summary>
       /// <param name="file"></param>
       /// <returns></returns>
       public static Image ConvertFileToImage(HttpPostedFileBase file)
       {
           var tempImage = new byte[file.ContentLength];
           file.InputStream.Read(tempImage, 0, file.ContentLength);
           var imageMemoryStream = new MemoryStream(tempImage);
           var returnImage = Image.FromStream(imageMemoryStream);
           var finalImage = ResizeImageWithAspectRatio(returnImage, 200, 200);
           return finalImage;
           
       }

       public static bool IsImage(this HttpPostedFileBase file)
       {
           return file.ContentType.Contains("image");
       }
    }
}
