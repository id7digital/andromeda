﻿using System;
using System.Linq;
using ID7Digital.EventBook.Domain.Enum;

namespace ID7Digital.EventBook.Domain.Helpers
{
    public static class PasswordStrengthHelper
    {
          /// <summary>
        /// Calculate the password strength
        /// If changes to this are made there is a corosponding javascript function that will need to be updated to match
        /// It is also in javascript to remove latency with a server check
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        public static PasswordStrengthEnum CalculatePasswordStrength(string password)
        {
            int len = password.Length;
            if (len >= 8)
            {
                var lowerCount = password.Count(char.IsLower);
                var upperCount = password.Count(char.IsUpper);

                if (len >= 10 && lowerCount > 0 && upperCount > 0)
                {
                    var numberCount = password.Count(char.IsDigit);
                    if (len >= 12 && numberCount > 0)
                    {
                        //Grab out just the letters and digits and compare to original, if there is something missing its a special character
                        var isVeryStrong = password.Any(x => !Char.IsLetterOrDigit(x)) && len >= 16;
                        
                        return isVeryStrong ? PasswordStrengthEnum.VeryStrong : PasswordStrengthEnum.Strong;
                    }
                    return PasswordStrengthEnum.Average;
                }
                return PasswordStrengthEnum.Weak;
            }
            return PasswordStrengthEnum.Invalid;
        }
    }
    
}
