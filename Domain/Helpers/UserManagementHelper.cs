﻿using System;
using System.Configuration;
using ID7Digital.EventBook.Domain.Abstract;

namespace ID7Digital.EventBook.Domain.Helpers
{
    public static class UserManagementHelper
    {
        /// <summary>
        /// Unlocks the account if it has been locked for the specified period of time.
        /// </summary>
        /// <param name="userName">Name of the user whose account we wanto to unlock.</param>
        /// <param name="AccountRepositoryRepo">The required repository to manage the user accounts.</param>
        /// <returns><c>true</c>, if the account has been unlocked. <c>false</c>, otherwise.</returns>
        public static bool TryUnlockAccountAfterWaitingPeriod(string userName, IAccountRepository AccountRepositoryRepo, int maxLockOutTimeInSeconds)
        {
            if (AccountRepositoryRepo != null)
            {
                var userAccount = AccountRepositoryRepo.FindAccountByUserName(userName);

                if (userAccount != null)
                {
                    if (userAccount.IsLocked)
                    {
                        if (userAccount.LastLockOutDate.HasValue && (DateTime.Now - userAccount.LastLockOutDate.Value).TotalSeconds >= maxLockOutTimeInSeconds)
                        {
                            userAccount.UnlockAccount();
                            UpdateLoginAttemptTracking(userAccount.UserName, true, AccountRepositoryRepo);
                            return true;
                        }

                        return false;
                    }

                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="validated"></param>
        /// <param name="AccountRepositoryRepo"></param>
        public static void UpdateLoginAttemptTracking(string userName, bool validated, IAccountRepository AccountRepositoryRepo)
        {
            if (AccountRepositoryRepo != null)
            {
                var userAccount = AccountRepositoryRepo.FindAccountByUserName(userName);

                if (userAccount != null)
                {
                    if (validated)
                    {
                        userAccount.FailedAttemptsCount = 0;
                    }
                    else
                    {
                        userAccount.FailedAttemptsCount++;
                        if (userAccount.FailedAttemptsCount == int.Parse(ConfigurationManager.AppSettings["maxInvalidLogginAttempts"]))
                        {
                            userAccount.LockAccount();
                        }

                    }
                    AccountRepositoryRepo.SaveChanges();
                }
            }
        }
    }
}
