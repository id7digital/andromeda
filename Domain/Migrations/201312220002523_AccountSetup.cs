namespace ID7Digital.EventBook.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AccountSetup : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        UserGuid = c.String(nullable: false, maxLength: 38),
                        Title = c.String(maxLength: 50),
                        FirstName = c.String(maxLength: 200),
                        LastName = c.String(maxLength: 200),
                        MiddleName = c.String(maxLength: 200),
                        DisplayName = c.String(maxLength: 200),
                        JobTitle = c.String(maxLength: 200),
                        Gender = c.Int(nullable: false),
                        ActivationCode = c.String(maxLength: 38),
                        ActivationReturnUrl = c.String(maxLength: 1000),
                        IsActivated = c.Boolean(nullable: false),
                        FailedAttemptsCount = c.Int(nullable: false),
                        IsLocked = c.Boolean(nullable: false),
                        LastLockOutDate = c.DateTime(),
                        AvatarFileName = c.String(maxLength: 200),
                        AvatarContentType = c.String(maxLength: 50),
                        DateRegistered = c.DateTime(nullable: false),
                        LastUpdated = c.DateTime(nullable: false),
                        PasswordResetCode = c.String(maxLength: 38),
                        AuthenticationEnabled = c.Boolean(nullable: false),
                        AuthenticationCode = c.String(),
                        TwoFactorAttempts = c.Int(nullable: false),
                        AuthenticateByEmail = c.Boolean(nullable: false),
                        AuthenticateBySMS = c.Boolean(nullable: false),
                        AuthenticateByPreGeneratedCode = c.Boolean(nullable: false),
                        TwoFactorIpAddressesEnabled = c.Boolean(nullable: false),
                        PasswordStrength = c.Int(nullable: false),
                        SynchronisationModifiedDate = c.DateTime(),
                        SyncronisationMapping = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        BuildingName = c.String(maxLength: 40),
                        BuildingNumber = c.String(),
                        ThoroughfareOrStreet = c.String(maxLength: 40),
                        Locality = c.String(maxLength: 40),
                        Town = c.String(maxLength: 40),
                        PostalCode = c.String(maxLength: 8),
                        County = c.String(maxLength: 40),
                        IsPrimaryAddress = c.Boolean(nullable: false),
                        SynchronisationModifiedDate = c.DateTime(),
                        SyncronisationMapping = c.String(),
                        Country_ID = c.Int(),
                        UserAccount_ID = c.Int(),
                        Account_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Countries", t => t.Country_ID)
                .ForeignKey("dbo.Accounts", t => t.UserAccount_ID)
                .ForeignKey("dbo.Accounts", t => t.Account_ID, cascadeDelete: true)
                .Index(t => t.Country_ID)
                .Index(t => t.UserAccount_ID)
                .Index(t => t.Account_ID);
            
            CreateTable(
                "dbo.Countries",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Description = c.String(maxLength: 100),
                        CountryCode = c.String(maxLength: 10),
                        SynchronisationModifiedDate = c.DateTime(),
                        SyncronisationMapping = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Addresses", new[] { "Account_ID" });
            DropIndex("dbo.Addresses", new[] { "UserAccount_ID" });
            DropIndex("dbo.Addresses", new[] { "Country_ID" });
            DropForeignKey("dbo.Addresses", "Account_ID", "dbo.Accounts");
            DropForeignKey("dbo.Addresses", "UserAccount_ID", "dbo.Accounts");
            DropForeignKey("dbo.Addresses", "Country_ID", "dbo.Countries");
            DropTable("dbo.Countries");
            DropTable("dbo.Addresses");
            DropTable("dbo.Accounts");
        }
    }
}
