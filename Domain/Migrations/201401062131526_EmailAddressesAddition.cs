namespace ID7Digital.EventBook.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EmailAddressesAddition : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EmailAddresses",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Email = c.String(nullable: false, maxLength: 200),
                        Description = c.String(maxLength: 200),
                        IsPrimary = c.Boolean(nullable: false),
                        EmailType = c.Int(nullable: false),
                        SynchronisationModifiedDate = c.DateTime(),
                        SyncronisationMapping = c.String(),
                        Account_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Accounts", t => t.Account_ID, cascadeDelete: true)
                .Index(t => t.Account_ID);
        }
        
        public override void Down()
        {
            DropIndex("dbo.EmailAddresses", new[] { "Account_ID" });
            DropForeignKey("dbo.EmailAddresses", "Account_ID", "dbo.Accounts");
            DropTable("dbo.EmailAddresses");
        }
    }
}
