namespace ID7Digital.EventBook.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FriendSettings : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Friends",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        MyFriendsAccountId = c.Int(nullable: false),
                        CreateDate = c.DateTime(),
                        SynchronisationModifiedDate = c.DateTime(),
                        SyncronisationMapping = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.FriendInvitations",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        Email = c.String(),
                        FriendInvitationGuid = c.Guid(),
                        CreateDate = c.DateTime(),
                        BecameAccountId = c.Int(nullable: false),
                        SynchronisationModifiedDate = c.DateTime(),
                        SyncronisationMapping = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.StatusUpdates",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        Status = c.String(nullable: false, maxLength: 500),
                        CreateDate = c.DateTime(),
                        SynchronisationModifiedDate = c.DateTime(),
                        SyncronisationMapping = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.StatusUpdates");
            DropTable("dbo.FriendInvitations");
            DropTable("dbo.Friends");
        }
    }
}
