﻿using ID7Digital.EventBook.Domain.Models;
using System.Data.Entity;

namespace ID7Digital.EventBook.Domain.Context
{
    public class EventBookContext : DbContext
    {
        public EventBookContext() : base("EventBookContext") { }

        public DbSet<Account> Accounts { get; set; }

        public DbSet<Address> Addresses { get; set; }

        public DbSet<Country> Countries { get; set; }

        public DbSet<EmailAddress> EmailAddresses { get; set; }

        public DbSet<Friend> Friends { get; set; }

        public DbSet<FriendInvitation> FriendInvitations { get; set; }

        public DbSet<StatusUpdate> StatusUpdates { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Country>().ToTable("dbo.Countries");

            modelBuilder.Entity<EmailAddress>().ToTable("dbo.EmailAddresses");

            modelBuilder.Entity<Account>().HasMany(x => x.Addresses).WithOptional().WillCascadeOnDelete();

            modelBuilder.Entity<Account>().HasMany(x => x.EmailAddresses).WithOptional().WillCascadeOnDelete();

            modelBuilder.Entity<Address>().HasOptional(x => x.Country);

            base.OnModelCreating(modelBuilder);
            
        }

    }
}
