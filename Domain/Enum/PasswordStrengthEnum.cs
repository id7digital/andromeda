﻿using System.ComponentModel;


namespace ID7Digital.EventBook.Domain.Enum
{
    public enum PasswordStrengthEnum
    {
        [Description("Invalid")]
        Invalid = 0,
        [Description("Weak")]
        Weak = 1,
        [Description("Average")]
        Average = 2,
        [Description("Strong")]
        Strong = 3,
        [Description("Very Strong")]
        VeryStrong = 4
    }
}
