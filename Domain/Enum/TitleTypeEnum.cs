﻿namespace ID7Digital.EventBook.Domain.Enum
{
    public enum TitleTypeEnum
    {
        Mr,
        Mrs, 
        Miss,
        Sir,
        Master
    }
}
