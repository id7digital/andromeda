﻿using System.Collections.Generic;
using System.Linq;
using ID7Digital.EventBook.Domain.Abstract;
using ID7Digital.EventBook.Domain.Models;

namespace ID7Digital.EventBook.Domain.Concrete
{
    public class StatusUpdateRepository : EventBookRepository<StatusUpdate>, IStatusUpdateRepository
    {
        public List<StatusUpdate> GetTopStatusUpdatesByAccountId(int accountId, int number)
        {
           return  UnderlyingSet.Where(x => x.AccountId == accountId).OrderBy(x => x.CreateDate).ThenByDescending(x => x.CreateDate).Take(number).ToList();       
        }
    }
}
