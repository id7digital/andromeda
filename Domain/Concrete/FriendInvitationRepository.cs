﻿using System;
using System.Collections.Generic;
using ID7Digital.EventBook.Domain.Abstract;
using ID7Digital.EventBook.Domain.Models;
using System.Linq;

namespace ID7Digital.EventBook.Domain.Concrete
{
    public class FriendInvitationRepository : EventBookRepository<FriendInvitation>, IFriendInvitationRepository
    {
        public FriendInvitation GetFriendInvitationByGuid(System.Guid? friendInvitationGuid)
        {
            return UnderlyingSet.FirstOrDefault(x => x.FriendInvitationGuid == friendInvitationGuid);
        }


        public void CleanUpFriendInvitationsForEmail(FriendInvitation friendInvitation)
        {
            var friendInvitations = UnderlyingSet.Where(x => x.Email == friendInvitation.Email && x.BecameAccountId == 0 && x.AccountId == friendInvitation.AccountId);

            foreach (var invitation in friendInvitations)
            {
                UnderlyingSet.Remove(invitation);
            }         
        }

        public bool CreateFriendRequest(int friendId, int userId)
        {
            var friendInvite = new FriendInvitation
                {
                    AccountId = userId,
                    BecameAccountId = friendId,
                    FriendInvitationGuid = Guid.NewGuid(),
                    CreateDate = DateTime.Now

                };

            var friendInviteObj = UnderlyingSet.Add(friendInvite);

            return friendInviteObj != null;
        }
    }
}
