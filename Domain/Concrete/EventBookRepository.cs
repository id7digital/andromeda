﻿using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using ID7Digital.EventBook.Domain.Abstract;
using ID7Digital.EventBook.Domain.Context;
using System.Data.Entity;

namespace ID7Digital.EventBook.Domain.Concrete
{
    public class EventBookRepository<T> : IEventBookRepository<T> where T : class, Models.IEBEntity
    {
        // database context linked to db
        private readonly EventBookContext _Context = new EventBookContext();

        protected DbSet<T> UnderlyingSet { get; set; }

        protected EventBookRepository()
        {
            UnderlyingSet = _Context.Set<T>();
        }

        // Add An entity to the context
        // - Context state Add
        // - Context.Entry(Entity).State = EntityState.Added
        public T Add(T entity)
        {
            var returnEnt = UnderlyingSet.Add(entity);

            SaveChanges();

            return returnEnt;
        }

        // Attach An entity to the context
        // - Context state Attach 
        public T Attach(T obj)
        {
            var returnEnt = UnderlyingSet.Attach(obj);
          
            SaveChanges();

            return returnEnt;
        }

        public TDerivedEntity Create<TDerivedEntity>() where TDerivedEntity : class, T
        {
            return UnderlyingSet.Create<TDerivedEntity>();
        }


        public T Create()
        {
            return UnderlyingSet.Create();
        }

        // retrieve all datasets
        public IQueryable<T> Fetch()
        {
            return UnderlyingSet;
        }

        // retrieve all datasets
        public IEnumerable<T> GetAll()
        {
            return UnderlyingSet;
        }

        // retreive entity based on ID
        public T GetByID(int ID)
        {
            return UnderlyingSet.Find(ID);
        }

        // remove entity that has been attached to the context
        public T Remove(T entity)
        {
            entity = UnderlyingSet.Remove(entity);
            SaveChanges();

            return entity;
        }

        // save changes to the database
        public void SaveChanges()
        {
            _Context.SaveChanges();
        }

    }
}
