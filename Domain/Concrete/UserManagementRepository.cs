﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Security;
using ID7Digital.EventBook.Domain.Abstract;
using ID7Digital.EventBook.Domain.Helpers;
using Ninject;

namespace ID7Digital.EventBook.Domain.Concrete
{
    public class UserManagementRepository : IUserManagementRepository
    {
        public IAccountRepository AccountRepositoryRepo { get; set; }

        public MembershipUser CreateUser(string userName, string password, string email = null)
        {
            MembershipUser returnUser = null;
            try
            {
                returnUser = Membership.CreateUser(userName, password, email);
            }
            catch (MembershipCreateUserException ex)
            {
                //TODO: Throw an exception to tell the user the email address is already in use.
                throw new ValidationException(ex.Message);
            }
            return returnUser;
        }

        public void DeleteUser(string userName)
        {
            Membership.DeleteUser(userName);
        }

        public void SetRolesForUser(string userName, IEnumerable<string> roles)
        {
            var userRoles = Roles.GetRolesForUser(userName);

            if (userRoles.Length != 0)
            {
                Roles.RemoveUserFromRoles(userName, userRoles);
            }

            if (roles.Any())
            {
                Roles.AddUserToRoles(userName, roles.ToArray());
            }
        }

        public IEnumerable<string> GetRolesForUser(string userName)
        {
            return Roles.GetRolesForUser(userName);
        }

        public IEnumerable<string> GetRoles()
        {
            return Roles.GetAllRoles();
        }

        public void CreateRole(string roleName)
        {
            try
            {
                Roles.CreateRole(roleName);
            }
            catch (Exception)
            {

            }
        }

        public void DeleteRole(string roleName)
        {
            try
            {
                Roles.DeleteRole(roleName);
            }
            catch (Exception)
            {

            }
        }

        public IEnumerable<string> GetUsers()
        {
            var items = Membership.GetAllUsers().OfType<MembershipUser>();
            return items.Select(x => x.UserName);
        }

        public IEnumerable<string> GetUsers(string filter)
        {
            var items = Membership.GetAllUsers().OfType<MembershipUser>();
            var query =
                from user in items
                where user.UserName.Contains(filter) ||
                      (user.Email != null && user.Email.Contains(filter))
                select user.UserName;
            return query;
        }

        public IEnumerable<string> GetUsersByRole(string role)
        {
            return Roles.GetUsersInRole(role);
        }

        public void SetSecretQuestionAndAnswer(string userName, string password, string question, string answer)
        {

            if (String.IsNullOrEmpty(userName))
            {
                throw new ValidationException("Username is required");
            }

            if (String.IsNullOrEmpty(password))
            {
                throw new ValidationException("Password is required");
            }

            try
            {
                var user = Membership.GetUser(userName);

                if (user != null)
                {
                    bool validated = user.ChangePasswordQuestionAndAnswer(password, question, answer);
                }
            }
            catch (MembershipPasswordException)
            {

            }
        }

        public bool ResetPasswordWithAnswer(string userName, string password, string answer)
        {
            var user = Membership.GetUser(userName);
            // Test that the given answer matches that of the one against the account
            try
            {
                if (user != null)
                {
                    user.ResetPassword(answer);
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                // Resetting with the answer has failed
                return false;
            }
            SetPassword(userName, password);
            return true;
        }

        public void SetPassword(string userName, string password)
        {
            if (String.IsNullOrEmpty(userName))
            {
                throw new ValidationException("Username is required");
            }
            if (String.IsNullOrEmpty(password))
            {
                throw new ValidationException("Password is required");
            }

            var provider = Membership.Provider;
            if (password.Length < provider.MinRequiredPasswordLength)
            {
                throw new ValidationException(String.Format("{0} is the minimum password length", provider.MinRequiredPasswordLength));
            }
            if (provider.MinRequiredNonAlphanumericCharacters > 0)
            {
                int num2 = 0;
                for (int i = 0; i < password.Length; i++)
                {
                    if (!char.IsLetterOrDigit(password[i]))
                    {
                        num2++;
                    }
                }
                if (num2 < provider.MinRequiredNonAlphanumericCharacters)
                {
                    throw new ValidationException(String.Format("{0} is the minimum number of non-alphanumeric characters", provider.MinRequiredNonAlphanumericCharacters));
                }
            }
            if (!String.IsNullOrWhiteSpace(provider.PasswordStrengthRegularExpression) &&
                !System.Text.RegularExpressions.Regex.IsMatch(provider.PasswordStrengthRegularExpression, password))
            {
                throw new ValidationException(String.Format("Password does not match the regular expression {0}", provider.PasswordStrengthRegularExpression));
            }

            try
            {
                var user = Membership.GetUser(userName);
                user.ChangePassword(user.ResetPassword(), password);

                if (AccountRepositoryRepo != null)
                {
                    var account = AccountRepositoryRepo.FindAccountByUserGuid(user.ProviderUserKey.ToString());
                    if (account != null)
                    {
                        account.PasswordStrength = PasswordStrengthHelper.CalculatePasswordStrength(password);
                    }
                }

                UserManagementHelper.UpdateLoginAttemptTracking(userName, true, AccountRepositoryRepo);
            }
            catch (MembershipPasswordException mex)
            {
                throw new ValidationException(mex.Message, mex);
            }
        }


        public MembershipUser GetExisitingUser(string userName)
        {
         return Membership.GetUser(userName);
        }


        public bool ExistingUser(string userName)
        {
          var users =  Membership.FindUsersByName(userName);

          return users.Count != 0;
        }

        public bool ExistingEmail(string email)
        {
            var users = Membership.FindUsersByName(email);

            return users.Count != 0;
        }
    }
}
