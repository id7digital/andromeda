﻿using System;
using System.Collections.Generic;
using ID7Digital.EventBook.Domain.Abstract;
using ID7Digital.EventBook.Domain.Models;
using ID7Digital.EventBook.Domain.Utilities;
using System.Linq;

namespace ID7Digital.EventBook.Domain.Concrete
{
    public class AccountRepository : EventBookRepository<Account>, IAccountRepository
    {   
        public IQueryable<Account> FindAllAccounts()
        {
            return UnderlyingSet;
        }

        public Account FindAccountById(int id)
        {
            return UnderlyingSet.FirstOrDefault(x => x.ID == id); 
        }

        public Account FindAccountByUserGuid(string userGuid)
        {
            return UnderlyingSet.FirstOrDefault(x => x.UserGuid == userGuid); 
        }

        public Account FindAccountByUserName(string userName)
        {
            return UnderlyingSet.FirstOrDefault(x => x.UserName == userName); 
        }

        public Account FindAccountByActivationGuid(string activationGuid)
        {
            return UnderlyingSet.FirstOrDefault(x => x.ActivationCode == activationGuid); 
        }

        public Account FindAccountByEmailAddress(string emailAddress)
        {
            return UnderlyingSet.FirstOrDefault(x => x.EmailAddresses.Any(y => y.Email.ToLower() == emailAddress.ToLower()));
        }

        public Account FindAccountByPrimaryEmailAddress(string emailAddress)
        {
            return UnderlyingSet.FirstOrDefault(x => x.EmailAddresses.Any(y => y.Email.ToLower() == emailAddress.ToLower() && y.IsPrimary));
        }

        public Account GetCurrentAccount()
        {
           if (CurrentUser.UserInformation != null && CurrentUser.IsLoggedIn)
           {
               return GetByID(CurrentUser.UserInformation.AccountId);
           }
           return null;   
        }

        public bool CreateAccount(Account account)
        {
            var createdAccount = Add(account);
            return createdAccount != null;
        }

        public void RemoveAccount(Account account)
        {
            Remove(account);
        }

        public bool UpdateAccount(Account account)
        {
          var returnEnt = Attach(account);
          
          return (returnEnt != null);
        }

        public Account FindAccountByPasswordResetCode(string passwordResetCode)
        {
            return UnderlyingSet.FirstOrDefault(x => x.PasswordResetCode == passwordResetCode);
        }

        public bool StartEmailResetPasswordProcess(string userName, string emailAddress, out string passwordResetCode)
        {
            passwordResetCode = string.Empty;

            var user = FindAccountByUserName(userName);

            if (user != null)
            {
                var email = user.GetPrimaryEmailAddress();

                if (email != null && email.Email == emailAddress)
                {
                    try
                    {
                        user.PasswordResetCode = Guid.NewGuid().ToString();
                        passwordResetCode = user.PasswordResetCode;
                        SaveChanges();
                        return true;
                    }
                    catch (Exception)
                    {                    
                        return false;
                    }
                }
            }

            return false;
        }

        public bool StartEmailResetPasswordProcess(string userName, string emailAddress)
        {
            string resetCode;
            return StartEmailResetPasswordProcess(userName, emailAddress, out resetCode);
        }

        public bool StartEmailResetPasswordProcess(int id)
        {
            var user = GetByID(id);

            if (user != null)
            {
                return StartEmailResetPasswordProcess(user.UserName, user.GetPrimaryEmailAddress().Email);
            }
            return false;
        }

        public List<Account> SearchAccounts(string searchText)
        {          
            //TODO: Add Email for Account Entity.

              return  UnderlyingSet.Where(
                    x =>
                    (x.FirstName + " " + x.LastName).Contains(searchText) || x.UserName.Contains(searchText)).Take(5).ToList();
        }
    }
}
