﻿using System.Collections.Generic;
using System.Linq;
using ID7Digital.EventBook.Domain.Abstract;
using ID7Digital.EventBook.Domain.Context;
using ID7Digital.EventBook.Domain.Models;

namespace ID7Digital.EventBook.Domain.Concrete
{
    public class FriendRepository : EventBookRepository<Friend>, IFriendRepository
    {

        public List<Friend> GetFriendsByAccountId(int accountId)
        {
            //Get my friends direct relationship
            var directFriendsList = UnderlyingSet.Where(x => x.AccountId == accountId && x.MyFriendsAccountId != accountId).Distinct();

            var fullFriendList = directFriendsList.ToList();

            ////Get my friends indirect relationship
            var indirectFriendsList = UnderlyingSet.Where(x => x.AccountId != accountId && x.MyFriendsAccountId == accountId).Distinct();

            foreach (var friend in indirectFriendsList)
            {
                if (friend != null)
                {
                    var tempFriendsAccountId = friend.MyFriendsAccountId;
                    var tempAccountId = friend.AccountId;
                    friend.AccountId = tempFriendsAccountId;
                    friend.MyFriendsAccountId = tempAccountId;
                    fullFriendList.Add(friend);

                }           
            }
            return fullFriendList;
        }

        public List<Account> GetFriendsAccountsByAccountId(int accountId)
        {
            var friends = GetFriendsByAccountId(accountId);

            var accountIDs = friends.Select(friend => friend.MyFriendsAccountId).ToList();

            using (var ctx = new EventBookContext())
            {
                IEnumerable<Account> accounts = ctx.Accounts.Where(x => accountIDs.Contains(accountId));
                var result = accounts.ToList();

                return result;
            }
          
        }

  
    }
}
