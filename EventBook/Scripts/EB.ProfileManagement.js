﻿//Revelaing Module pattern used to encapsulate javascript

var EB = (function() {


    var areas = [];

    // click event to obtain element and area
    var init = function() {

        areas["/AccountManagement/ProfileManagement"] = profileManagement;
        areas["/AccountManagement/ProfileContactInfo"] = profileContactInfo;
        areas["/AccountManagement/AdditionalContactInformation"] = additionalProfileContactInfo;
        areas["/AccountManagement/ProfileAddress"] = profileAddress;
        areas["/AccountManagement/ProfileAvatar"] = profileAvatar;


        $('ul#menu a').bind('click', function (e) {
            var element = $(this),
                area = element.data('area');
            loadArea(area, element);

            e.preventDefault();
            return false;
        });

        console.log('loadInit . .');

    };

    // pass the element and area to loadArea where ajax retrieves the html and loads it 
    // then initArea initialises the event handlers
    var loadArea = function(area, element) {

        if (element && $(element).data('redirect')) {
            // redirect
            window.location = window.location.protocol + "//" + window.location.host + area;
        } else {

        $.ajax({
            url: area,
            type: 'GET',
            success: function(data) {
              
                    $('.main-content').fadeOut(100, function() {
                        $('.main-content').html('<div>' + data.Html + '</div>').fadeIn(250, function() {
                        //initialise event handlers
                        // set unobtrusive parse to allow for validate of form
                            initArea(area);
                           // EB.ParseValidation('section.container');
                    });


                });
              

            }
        });
        }
        console.log('loadArea. . .');
    };

    // Initialises event handlers
    var initArea = function(area) {
        if (areas[area])
            areas[area].init();
        
        console.log('Initialize Area. . .');
    };


    var profileManagement= function() {

        var init = function() {

            $('#').unbind();
            $('#').bind('click', function() {
            });
            
            console.log('load Profile Management Button Clicks . .');
        };

        return {
            init: init
        };
    }();

    var profileContactInfo = function () {

        var init = function () {

            Common.ContactInformaion();

            console.log('load Profile Management Button Clicks . .');
        };

        return {
            init: init
        };
    }();
    
    var additionalProfileContactInfo = function () {

        var init = function () {

            $('#').unbind();
            $('#').bind('click', function () {
            });

            console.log('load Profile Management Button Clicks . .');
        };

        return {
            init: init
        };
    }();
       
    
    var profileAddress = function () {

        var init = function () {

            Common.ProfileAvatar();

            console.log('load Profile Management Button Clicks . .');
        };

        return {
            init: init
        };
    }();

    var profileAvatar = function () {

        var init = function () {

            Common.ProfileAvatar();

            console.log('load Profile Management Button Clicks . .');
        };

        return {
            init: init
        };
    }();
    
    var parseValidation = function (selector) {
        $.validator.unobtrusive.parseDynamicContent(selector);
    };

    return {
        init: init,
        InitArea: initArea,
        //ParseValidation: parseValidation,
    };


})();