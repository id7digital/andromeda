﻿var Common = (function() {

    var contactInformaion = function () {

        // BIND: not using BIND event as it will attach an event handler to all selectors found on page load.
        // LIVE: LIVE binds the event handler to its selector at document level, attaching event handler once instead of multiple times.
        //       Depreciated since jquery1.7
        // ON: do not have to attach handlers at document level but rather a parent selector of your choice.
        
        $('.contact-Information-Form').live('submit', function (e) {

            var contactInformationForm = $(this);
            var section = contactInformationForm.data('section');
            
            // Add a hidden field for obtaining the title as a string value.
            
            var title = $("#Title option:selected").text();
            var titleObj = contactInformationForm.find("#Title");
            
            titleObj.text(title);
            var data = contactInformationForm.serialize() + "&" + $.param({ section: section });

            $.ajax({
                url: contactInformationForm.attr('action'),
                data: data,
                type: "POST",
                success: function (result) {
                    if (result.Success) {
                        createNotice('success', 'Profile Updated');
                        
                        $('.main-content').html(result.Html);
                        
                        
                        //Common.ParseValidation('#AreaContent');
                    }
                    else {
                        createNotice('error', 'Error Updating Profile');
                    }
                }
            });

            e.preventDefault();
            return false;
        });
        
        $('.profile-Management input[type="button"]', '#ProfileManagement').live('click', function (e) {
            var accountInfoclassName = '.profile-Management';
            toggleEditArea(this, accountInfoclassName);
        });
        
        return {
            ContactInformaion: contactInformaion
        };

    }();
    
    var toggleEditArea = function (element, className) {
        if (!$(element).is("form")) {
            element = $(element).parents("form");
        }

        var current = $(className + ":visible", element);
        var target = $(className + ".hiddenInput", element);

        //Toggle state
        current.addClass('hiddenInput');
        $(target).removeClass('hiddenInput');
    };

    
    var onClose = function () {
    };

    var createNotice = function (noticeType, noticeMessage, timeout, onClose) {
        timeout = timeout != undefined ? timeout : 3000;
        noty({
            layout: 'bottomRight',
            theme: 'noty_theme_default',
            type: noticeType,
            text: noticeMessage,
            timeout: timeout,
            callback: {
                onClose: onClose
            }
        });
    };

    var passwordManagement = function() {

        // Event to deal with password strength checks
        $('#Password').live('keyup', function () {
            var password = $(this).val();

            updatePasswordStrengthVisibility(this);

            var strength = checkPasswordStrength(password);
            var displayMessage = strength;
            var percentage = 0;
            switch (strength) {
            case "Weak":
                percentage = 0.25;
                break;
            case "Average":
                percentage = 0.50;
                break;
            case "Strong":
                percentage = 0.75;
                break;
            case "VeryStrong":
                percentage = 1;
                displayMessage = "Very Strong";
                break;
            default:
            }

            $("#PasswordStrengthStatus").html(displayMessage + " Password");
            var totalWidth = $('#PasswordStrengthProgressContainer').width();

            var width = percentage == 0 ? 0 : totalWidth * percentage;
            $('#PasswordStrengthProgress').css('width', width);

            $('#PasswordStrengthProgress').removeClass().addClass(strength);
        });

        var updatePasswordStrengthVisibility = function(input) {
            var container = $("#PasswordStrengthContainer");
            if ($(input).val() != "") {
                container.show();
            } else if (container.is(":visible")) {
                container.hide();
            }
        };
        

        // Check the password strength
        // If this is changed please reflect the changes into the password strength helper
        function checkPasswordStrength(password) {
            var len = password.length;
            if (len >= 8) {
                var hasUpper = /[A-Z]/.test(password);
                var hasLower = /[a-z]/.test(password);

                if (len >= 10 && hasLower && hasUpper) {
                    var hasNumber = /[0-9]/.test(password);
                    if (len >= 12 && hasNumber) {
                        var hasSpecial = /[^a-zA-Z0-9 ]+/.test(password);

                        if (len >= 16 && hasSpecial) {
                            return "VeryStrong";
                        }
                        return "Strong";
                    }
                    return "Average";
                }
                return "Weak";
            }
            return "Invalid";
        }

        return {
            PasswordManagement: passwordManagement
        };

    }();


    var profileAvatar = function () {
         
            $("#ImageSelection").live('change', function () {
                var filePathParts = $(this).val().split(".");
                var ext = filePathParts[filePathParts.length - 1].toLowerCase();

                if (ext == "jpg" || ext == "png" || ext == "jpeg" || ext == "gif") {
                    $('#ImageUploadFrame').one('load', function (e) {
                        refreshAvatar();
                        CreateNotice('success', 'Avatar successfully updated');
                    });

                    var form = $(this).parents("form");
                    //form.append(iframe);

                    form.submit();
                } else {
                    CreateNotice('error', 'Invalid image type');
                }
                $("#ImageSelection").val('');
            });

            // Change avatar
            $("#ChangeAvatar").live('click', function (e) {
                var input = $("#ImageSelection");

                input.show();
                $(input).focus().click();
                input.hide();

                e.preventDefault();
                return false;
            });

            // Remove avatar
            $("#RemoveAvatar").live('click', function () {
                var id = $("#ID", "#EditAvatarImage").val();
                $.ajax({
                    url: '/AccountManagement/RemoveAvatar',
                    cache: false,
                    data: { id: id },
                    success: function () {
                        refreshAvatar();
                    }
                });
            });

            // Refresh avatar image
            var refreshAvatar = function (refreshTitleBar) {
                var id = $("#ID", "#EditAvatarImage").val();
                var element;
                var data = { id: id };

                if (refreshTitleBar) {
                    data.width = 40;
                    data.height = 40;
                    element = $("#AvatarDisplay", "header");
                } else {
                    element = $("#AvatarDisplay", "#EditAvatarImage");
                    refreshAvatar(true);
                }

                $.ajax({
                    url: '/AccountManagement/ViewAvatar',
                    cache: false,
                    data: data,
                    success: function (data) {
                        element.html(data);
                    }
                });
            };
        
        return {
            ProfileAvatar: profileAvatar
        };
    }();

    var searchUsers = function () {
          
        $('.search-user-base').live('focusin', function () {
            $(this).val('');
        });
        
        $('.search-user-base').live('focusout', function () {
            $(this).val('');
        });

        
        $('.search-user-base').live('keyup', function() {
            var searchBox = $(this);
            var searchedValue = searchBox.val();
            var searchBoxUrl = searchBox.data('area');
            
            

            $.ajax({
                url: searchBoxUrl,
                type: "POST",
                data: { searchedUser: searchedValue },
                async: true,
                success: function(result) {
                    if (result.Success) {

                        $('.main-content').html(result.Html);
                    }
                },               
            });

        });

        return {
            SearchUsers: searchUsers
        };
    }();

    var addFriend = function () {

        $('#AddFriendButton').live('click', function() {

            var addFriendObj = $(this);
            var friendUrl = addFriendObj.data('url');
            var friendId = addFriendObj.data('friend');
            var myUserId = addFriendObj.data('user');
            
            $.ajax({
                url:friendUrl,
                type:"POST",
                data: { friendId: friendId, userId: myUserId },
                success: function(data) {
                    
                }
                
            });            
            
            
        });
        
        return {
            AddFriend: addFriend
        };
    }();


})();