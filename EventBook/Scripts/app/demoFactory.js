﻿appRoot.factory('demoFactory', ['$http', function ($http) {

    var urlBase = '/api/user';
    var demoFactory = {};

    demoFactory.getCustomers = function () {
        return $http.get(urlBase);
    };

    demoFactory.getCustomer = function (id) {
        return $http.get(urlBase + '/' + id);
    };

    demoFactory.insertCustomer = function (cust) {
        return $http.post(urlBase, cust);
    };

    demoFactory.updateCustomer = function (cust) {
        return $http.put(urlBase + '/' + cust.ID, cust)
    };

    demoFactory.deleteCustomer = function (id) {
        return $http.delete(urlBase + '/' + id);
    };

    return demoFactory;

}]);