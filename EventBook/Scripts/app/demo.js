﻿
//function Animal(age, name) {
//     this.Age = age,
//    this.Name = name;

//    Animal.prototype.Eat = function (eating) {
//        $scope.Eating = eating;
//    };
//};

//function Lion() {
//    var age;
//    var name;
//    this.Roar = "Roaaar!";

//    Animal.call(this, age, name);
//};

//Lion.prototype = Object.create(Animal.prototype);


appRoot.controller('DemoController', ['demoFactory', '$scope', '$route', '$routeParams', '$location', function (demoFactory, $scope, $route, $routeParams, $location) {

    $scope.status;
    $scope.selectedUsers = [];
    $scope.orders;

    getCustomers();

    function getCustomers() {
        demoFactory.getCustomers()
                .success(function (custs) {
                    $scope.selectedUsers = custs;
                })
                .error(function (error) {
                    $scope.status = 'Unable to load customer data: ' + error.message;
                });
    };



   
    $scope.$watchCollection('selectedUsers', function () {
        $scope.selectedUser = angular.copy($scope.selectedUsers[0]);
    });

    $scope.userGrid = {
        data: 'usersList',
        multiSelect: false,
        selectedItems: $scope.selectedUsers,
        enableColumnResize: false,
        columnDefs: [
            { field: 'firstName', displayName: 'First Name', width: '25%' },
            { field: 'lastName', displayName: 'Last Name', width: '25%' },
            { field: 'email', displayName: 'Email', width: '25%' },
            { field: 'mobile', displayName: 'Mobile Number', width: '25%' }
        ]
    };

    $scope.updateUser = function (user) {
        userResource.update(user, function (updatedUser) {
            $scope.selectedUsers[0].id = updatedUser.id;
            $scope.selectedUsers[0].firstName = updatedUser.firstName;
            $scope.selectedUsers[0].lastName = updatedUser.lastName;
            $scope.selectedUsers[0].gender = updatedUser.gender;
            $scope.selectedUsers[0].mobile = updatedUser.mobile;
            $scope.selectedUsers[0].email = updatedUser.email;
            $scope.selectedUsers[0].city = updatedUser.city;
            $scope.selectedUsers[0].state = updatedUser.state;
            $scope.selectedUsers[0].country = updatedUser.country;
            $scope.selectedUsers[0].zip = updatedUser.zip;
        });
    };


    $scope.$watch('selectedUsers[0].state', function (selectedStateId) {
        if (selectedStateId) {
            angular.forEach($scope.countryList[0].states, function (state) {
                if (selectedStateId == state.id) {
                    $scope.selectedState = state;
                }
            });
        }
    });

    var init = function () {

    }

    init();
}]);