﻿using ID7Digital.EventBook.Domain.Models;
using ID7Digital.EventBook.Domain.Utilities;
using System.Web.Mvc;
using ID7Digital.EventBook.Services.Abstract;
using ID7Digital.EventBook.UI.Shared;

namespace ID7Digital.EventBook.UI.Controllers
{
    public class AccountBaseController : EBBaseController
    {
        public ActionResult SignOut()
        {        
            // Clear our session data ja
            CurrentUser.ClearSessionData();

            return RedirectToAction("Index", "Event");
        }

        //
        // GET: /AccountBase/a
        public ActionResult SignInPortal(string userName, string password, string returnUrl, bool isPersistent, Account account = null)
        {
            if (account != null)
            {
                CurrentUser.UserInformation = new LoggedInUser(account);
            }
          
            if (account != null && !string.IsNullOrEmpty(account.ActivationReturnUrl))
            {
                return Redirect(account.ActivationReturnUrl);
            }

            if (!string.IsNullOrWhiteSpace(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return RedirectToAction("Index", "AccountManagement");
        }

        /// <summary>
        /// Get the account object for currently logged in user
        /// </summary>
        /// <param name="accountRepo"></param>
        /// <returns></returns>
        public Account GetCurrentAccount(IAccountService accountRepo)
        {
            if (CurrentUser.UserInformation != null && CurrentUser.IsLoggedIn)
            {
                return accountRepo.GetAccountById(CurrentUser.UserInformation.AccountId);
            }

            return null;
        }

    }
}
