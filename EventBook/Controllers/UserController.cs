﻿using ID7Digital.EventBook.UI.Models;
using System.Collections.Generic;
using System.Web.Http;

namespace ID7Digital.EventBook.UI.Controllers
{
    public class UserController : ApiController
    {
        List<User> usersData = new List<User>()
        {
            new User{Id=1, FirstName="John", LastName="Smith", Gender=Gender.Male,Mobile="9999999991",Email="john@demo.com", City="kn", State="as", Country="usa",Zip="12401"},
            new User{Id=2, FirstName="Adam", LastName="Gril",Gender=Gender.Female, Mobile="9999999992",Email="adam@demo.com", City="bk",State="al", Country="usa",Zip="99701"},
            new User{Id=3, FirstName="James", LastName="Franklin",Gender=Gender.Male, Mobile="9999999993",Email="james@demo.com", City="js",State="nj", Country="usa",Zip="07097"},
            new User{Id=4, FirstName="Vicky", LastName="Merry" ,Gender=Gender.Female, Mobile="9999999994",Email="vicky@demo.com", City="ol",State="ny", Country="usa",Zip="14760"},
            new User{Id=5, FirstName="Cena", LastName="Rego",Gender=Gender.Male, Mobile="9999999995",Email="cena@demo.com", City="as",State="tx", Country="usa",Zip="78610"}
        };
        // GET api/user
        public IEnumerable<User> Get()
        {
            return usersData;
        }

        // GET api/user/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/user
        public void Post([FromBody]string value)
        {
        }

        // PUT api/user/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/user/5
        public void Delete(int id)
        {
        }
    }
}
