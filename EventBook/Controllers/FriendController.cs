﻿using System.Web.Helpers;
using System.Web.Mvc;
using ID7Digital.EventBook.Services.Abstract;
using ID7Digital.EventBook.UI.Shared;

namespace ID7Digital.EventBook.UI.Controllers
{
    using System.Linq;

    public class FriendController : EBBaseController
    {
        //
        // GET: /Friend/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult SearchUser(string searchedUser, IAccountService accountService)
        {
            var success = false;
            var html = string.Empty;

            var matchedUsers = accountService.SearchAccounts(searchedUser);

            if (matchedUsers != null)
            {

                matchedUsers = matchedUsers.Where(x => x.UserName != null).ToList();

                html = PartialViewToString(@"Friends", matchedUsers);
                              
                success = true;
            }

            return Json(new {Success = success, Html = html}, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Friends()
        {
           // return View();
            return null;
        }

        //[HttpPost]
        //public ActionResult AddFriend(int friendId, int userId, IFriendInviteService friendInviteService)
        //{
        //    var requestMade = friendInviteService.CreateFriendRequest(friendId, userId);

        //    if (requestMade)
        //    {
                
        //    }
        //    // return View();
        //    return null;
        //}

     

    }
}
