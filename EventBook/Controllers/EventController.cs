﻿
using ID7Digital.EventBook.Domain.Abstract;
using ID7Digital.EventBook.Domain.Models;
using ID7Digital.EventBook.Services.Abstract;
using ID7Digital.EventBook.Services.Concrete;
using ID7Digital.EventBook.Services.ViewModels;
using ID7Digital.EventBook.UI.Shared;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Net.Http;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Host.SystemWeb;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using Microsoft.Owin;
using System.Web;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;


namespace ID7Digital.EventBook.UI.Controllers
{

    public class EventController : EBBaseController
    {
        // GET: /Event////
        public ActionResult Index()
        {
            return View();
        }

        // CREATE: /Event/
        public ActionResult SignUp()
        {
           var signUpModel = new SignUpModel();

           return View(signUpModel);
        }

        // CREATE: /Event/
        [HttpPost]
        public ActionResult SignUp(SignUpModel signUpModel, IAccountService accountService, IUserService userManagementService)
        {        
            if(ModelState.IsValid)
            {
                var userNameLower = signUpModel.UserName.ToLower();
                var user = userManagementService.CreateUser(userNameLower, signUpModel.Password, signUpModel.Email);

                var hasAccountBeenCreated = accountService.CreateAccount(signUpModel);

                if (hasAccountBeenCreated && user != null)
                {
                    var activationModel = new ActivationEmailModel(signUpModel);
                    // send verification email     
                    AccountService.SendActivationPartialToEmail(activationModel, "Activate your EventBook Account", PartialViewToString("../Event/ActivationEmail", activationModel));
                                       
                    return View("InitialRegistration");
                }               
                
            }
            return View(signUpModel);
          
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="activationCode"></param>
        /// <param name="accountService"></param>
        /// <returns></returns>
        public ActionResult Activate(string activationCode, IAccountService accountService)
        {
            var account = accountService.FindAccountByActivationGuid(activationCode);

            if (account != null)
            {
              var activationCompleteViewModel = accountService.ActivateAccount(account);
             
                if (activationCompleteViewModel.ActivationCompleted)
                {
                    var emailModel = new ActivationEmailModel(account);

                    AccountService.SendActivationPartialToEmail(emailModel, "Welcome to EventBook", PartialViewToString("../Event/WelcomeEmail", emailModel));

                    return View("EventBookActivated", activationCompleteViewModel);
                }         
                    return RedirectToAction("Index", "Event");       
            }

             return View("EventBookActivated");
        }


        /// <summary>
        /// ResetPasswordFromEmail: 
        /// </summary>
        /// <param name="resetPasswordCode"></param>
        /// <param name="accountRep"></param>
        /// <returns></returns>
        public ActionResult ResetPasswordFromEmail(string resetPasswordCode, IAccountService accountRep, IUserService userManagementService)
        {
            var account = accountRep.FindAccountByPasswordResetCode(resetPasswordCode);

            if (account != null)
            {
                var passModel = new ResetPasswordModel
                {
                    UserName = account.UserName
                };

                var user = userManagementService.GetExisitingUser(account.UserName);

                if (user != null)
                {
                    return View("SetPassword", passModel);
                }
            }

            return View("InvalidPasswordResetLink");
        }

        ///// <summary>
        ///// SetPassword:
        ///// </summary>
        ///// <param name="newPasswordModel"></param>
        ///// <param name="managRep"></param>
        ///// <param name="accountRep"></param>
        ///// <returns></returns>
        [HttpPost]
        public ActionResult SetPassword(ResetPasswordModel newPasswordModel, IUserService managRep, IAccountService accountRep)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (newPasswordModel.NewPassword == newPasswordModel.ConfirmPassword && !string.IsNullOrEmpty(newPasswordModel.SecretQuestionAnswer))
                    {
                        var success = managRep.ResetPasswordWithAnswer(newPasswordModel.UserName, newPasswordModel.NewPassword, newPasswordModel.SecretQuestionAnswer);

                        if (success)
                        {
                            accountRep.FindAccountByUserName(newPasswordModel.UserName).PasswordResetCode = "";
                            Account userAccount = accountRep.FindAccountByUserName(newPasswordModel.UserName);
                            userAccount.UnlockAccount();
                            accountRep.SaveChanges();

                            return RedirectToAction("SignIn", "AccountManagement");
                        }
                        else
                        {
                            ModelState.AddModelError("SetPassword", "Password change failed, please check your secret answer.");
                        }
                    }
                }
                catch (ValidationException ex)
                {
                    ModelState.AddModelError("SetPassword", ex.Message);
                }
            }

            newPasswordModel.NewPassword = String.Empty;
            newPasswordModel.ConfirmPassword = String.Empty;
            //  return View(newPasswordModel);
            return null;
        }

        public ActionResult Demo()
        {
            return View();
        }

  

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Event"));
        }

        [AllowAnonymous]
        public ActionResult ExternalLoginCallback()
        {
            var externalLoginInfo = AuthenticationManager.GetExternalLoginInfo();
            var claims = externalLoginInfo.ExternalIdentity.Claims.ToList();

            //The second parameter must match the authentication type configured in the startup file.
            var id = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(id);
            var access_token = claims.FirstOrDefault(x => x.Type == "FacebookAccessToken").Value;
           // var fb = newFacebookClient(access_token);
            return RedirectToAction("index");

        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }


        private class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                context.RequestContext.HttpContext.Response.SuppressFormsAuthenticationRedirect = true;

                var properties = new AuthenticationProperties() { RedirectUri = RedirectUri };
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }

    }
}
