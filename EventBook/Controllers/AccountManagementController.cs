﻿using ID7Digital.EventBook.Domain.Enum;
using ID7Digital.EventBook.Domain.Helpers;
using ID7Digital.EventBook.Domain.Models;
using ID7Digital.EventBook.Services.Abstract;
using ID7Digital.EventBook.Services.Concrete;
using ID7Digital.EventBook.Services.ViewModels;
using ID7Digital.EventBook.UI.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ID7Digital.EventBook.UI.Controllers
{
    public class AccountManagementController : AccountBaseController
    {
        public ActionResult Index(IAccountService accountService)
        {
            var account = GetCurrentAccount(accountService);
            return View("ProfileManagement/Details", account);
        }


        public ActionResult SignIn(string returnUrl, bool mobile = false)
        {
            var vm = new SignInModel
            {
                ReturnUrl = returnUrl,
                ShowClientCertificateLink = false,
                IsSigninRequest = mobile
            };

            return View();
        }

        [HttpPost]
        public ActionResult SignIn(SignInModel model, IAccountService accountRepo, IUserService userManagementService, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var userAccount = accountRepo.FindAccountByUserName(model.UserName);
                var validLogin = userAccount != null && Membership.ValidateUser(model.UserName.ToLower(), model.Password); ;

                if (userAccount != null)
                {
                    // Check if the account is locked. If so, try to unlock it (if 24 hours have passed).
                    if (userAccount.IsLocked && accountRepo.TryUnlockAfterWaitingPeriod(userAccount.UserName, 86400))
                    {
                        ModelState.AddModelError("", "Account Locked. It will be unlocked automatically after 24 hours or through password reset");

                        return View(model);
                    }

                    // If, after the previous validation, the account is finally unlocked, proceed.
                    if (!userAccount.IsLocked)
                    {
                        if (validLogin)
                        {
                            if (userAccount.IsActivated)
                            {
                                accountRepo.UpdateLoginAttemptTracking(userAccount.UserName, true);

                                return SignInPortal(
                                    userAccount.UserName,
                                    model.Password,
                                    model.ReturnUrl,
                                    model.EnableSSO,
                                    userAccount);
                            }

                            return GetActivationLinkResendView(userAccount.UserName, accountRepo, null);

                        }

                        ModelState.AddModelError("", "Your username or password was entered incorrectly.");

                        var maxAttempts = int.Parse(ConfigurationManager.AppSettings["maxInvalidLogginAttempts"]);

                        var remainingAttempts = maxAttempts - userAccount.FailedAttemptsCount;

                        if (remainingAttempts <= 1)
                        {
                            var message = remainingAttempts == 1
                                              ? "Account will be locked with next failed login attempt"
                                              : "Account is locked and it will unlock automatically after 24 hours";
                            ModelState.AddModelError("", message);
                        }

                        accountRepo.UpdateLoginAttemptTracking(userAccount.UserName, false);

                    } // End user not locked
                }
                else
                {
                    ModelState.AddModelError("", "Your username or password was entered incorrectly.");
                } // End user account not null f
            } // End valid ModelState
            else
            {
                ModelState.AddModelError("", "Your username or password was entered incorrectly.");

            }

            return View(model);
        }

        [HttpPost]
        public JsonResult SaveProfile(string section, Account account, IAccountService accountService)
        {
            var success = false;
            var partialContent = string.Empty;

           var accountEntity = accountService.GetAccountById(account.ID);


            switch (section.ToLower())
            {
                case "contactinformation" :
                accountEntity.FirstName = account.FirstName;
                accountEntity.LastName = account.LastName;
                accountEntity.JobTitle = account.JobTitle;
                accountEntity.DisplayName = account.DisplayName;
                break;
                case  "Details": 
                break;
                case  "AdditionalContactInformation":
                  break;
                case "ProfileAddress":
                break;
                case "ProfilePicture":
                break;
             

            }
            accountService.SaveChanges();

            if (accountEntity != null)
            {                     
                    // Change the last update date
                    account.LastUpdated = DateTime.Now;

                    // Add in the titles view data
                    if (section.ToLower() == "contactinformation")
                    {
                        // add in titles view data ...
                        GetSelectListFromEnumTitleType(account);
                    }

                    // Render partial view to be sent back to replace part of the profile edit
                    partialContent = PartialViewToString(@"ProfileManagement\" + section, account);
                success = true;
            }

            return Json(new
            {
                Success = success,
                Html = partialContent,
            }, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult ProfileManagement(IAccountService accountService)
        {
            var account = GetCurrentAccount(accountService);

            var partialViewToString = PartialViewToString(@"ProfileManagement\Details", account);

            return Json(new { Html = partialViewToString }, JsonRequestBehavior.AllowGet);
        }

  

        [HttpGet]
        public JsonResult ProfileContactInformation(IAccountService accountService)
        {
            var account = GetCurrentAccount(accountService);

            GetSelectListFromEnumTitleType(account);

            var partialViewToString = PartialViewToString(@"ProfileManagement\ContactInformation", account);

            return Json(new { Html = partialViewToString }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult AdditionalContactInformation(IAccountService accountService)
        {
            var account = GetCurrentAccount(accountService);

            GetSelectListFromEnumTitleType(account);

            var partialViewToString = PartialViewToString(@"ProfileManagement\AdditionalContactInformation", account);

            return Json(new { Html = partialViewToString }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult ProfileAddress(IAccountService accountService)
        {
            var account = GetCurrentAccount(accountService);

            var partialViewToString = PartialViewToString(@"ProfileManagement\ProfileAddress", account);

            return Json(new { Html = partialViewToString }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult ProfilePicture(IAccountService accountService)
        {
            var account = GetCurrentAccount(accountService);

            var partialViewToString = PartialViewToString(@"ProfileManagement\ProfilePicture", account);

            return Json(new { Html = partialViewToString }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ResetPasswordRequest()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ResetPasswordRequest(ActivationEmailModel emailModel, IAccountService accountRepo, IUserService userManagementService)
        {
            if (ModelState.IsValid)
            {
                string resetCode;
                var userAccount = accountRepo.FindAccountByUserName(emailModel.UserName);

                var resetSuccess = accountRepo.StartEmailResetPasswordProcess(emailModel.UserName, emailModel.Email, out resetCode);

                if (resetSuccess)
                {
                    emailModel.FirstName = userAccount.FirstName;

                    var resetEmailSuccess = ResetPasswordRequestSendEmail(emailModel, resetCode);

                    if (resetEmailSuccess)
                    {
                        return View("EventBookPasswordResetEmailSent");
                    }
                }

                ModelState.AddModelError("ResetPasswordRequest",  "The specified user name or email address is invalid.");
            }

            return View();
        }

        private ActionResult GetActivationLinkResendView(string userName, IAccountService accountRepo, IUserService userManagementRepository)
        {
            var userAccount = accountRepo.FindAccountByUserName(userName);
            var timeDifferenceFromSignUpIn = DateTime.Now.Subtract(userAccount.DateRegistered);

            if (timeDifferenceFromSignUpIn.TotalMinutes >= Double.Parse(ConfigurationManager.AppSettings["MinutesTillDeactiveAccount"]))
            {
                // Delete Account Record 
                accountRepo.RemoveAccount(userAccount);
                accountRepo.SaveChanges();

                //Delete User Record / Membership record                         
                userManagementRepository.DeleteUser(userName);
                return View("AccountExpired");
            }
            else
            {
                ViewData["UserName"] = userName;
                return View("ActivationLinkResend");
            }

        }

        
        public ActionResult AccountOf(string userName, IAccountService accountService)
        {
            var account = accountService.FindAccountByUserName(userName);

           return null;
        }

        public JsonResult IsValidNewUserName(string UserName, IUserService userManagementRepository)
        {
            var isUserExistent = userManagementRepository.ExistingUser(UserName);
            
            // If the new userName is already in use return false.
            return new JsonResult { Data = !isUserExistent, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult IsValidNewEmail(string Email, IUserService userManagementRepository)
        {
            var isUserExistent = userManagementRepository.ExistingEmail(Email);

            // If the new email is already in use return false.
            return new JsonResult { Data = !isUserExistent, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public ActionResult GetAvatarContent(IAccountService accountRepo, int ID)
        {
            var account = accountRepo.GetAccountById(ID);
            return GetAvatarContent(account);
        }


        [HttpPost]
        public void SaveAvatar(int ID, IAccountService accountRepo, HttpPostedFileBase image)
        {
            var account = accountRepo.GetAccountById(ID);
            var extention = Path.GetFileName(image.ContentType);
             

            if (image.IsImage())
            {
                string fileName = string.Format("{0}_{1}.{2}", account.ID, account.UserName, extention);
                var imageName = string.Format("{0}{1}", "~/App_Data/Avatars/", fileName);

                var finalImage = ImageHelper.ConvertFileToImage(image);
                finalImage.Save(imageName);

                // Save against the acount
                account.AvatarFileName = fileName;
                account.AvatarContentType = image.ContentType;
                accountRepo.SaveChanges();
            }
        }

        public void RemoveAvatar(int ID, IAccountService accountRepo, HttpPostedFileBase image)
        {
            var account = accountRepo.GetAccountById(ID);
            if (account != null)
            {
                account.AvatarFileName = string.Empty;
                account.AvatarContentType = string.Empty;
                accountRepo.SaveChanges();
            }
        }

        public ActionResult GetAvatar(IAccountService accountRepo, string userGuid)
        {
            var account = accountRepo.FindAccountByActivationGuid(userGuid);
            return GetAvatarContent(account);
        }

        private ActionResult GetAvatarContent(Account account)
        {
            var dir = Server.MapPath("/Content/images/");
            var contentType = "image/jpg";
            var fileName = "avatar.jpg";

            if (account != null && account.IsAvatarOverriden)
            {
                fileName = account.AvatarFileName;
                contentType = account.AvatarContentType;
                dir = "~/App_Data/Avatars/";
            }

            var path = Path.Combine(dir, fileName);
            return File(path, contentType);
        }
        public ActionResult ViewAvatar(int ID, int width = 200, int height = 200)
        {
            var imageDisplay = new AvatarDisplayModel
            {
                UserId = ID,
                Width = width,
                Height = height
            };

            return View(imageDisplay);
        }

        /// <summary>
        /// test
        /// </summary>
        /// <param name="account"></param>
        private void GetSelectListFromEnumTitleType(Account account)
        {
            // Check to see if enum has been set if notset it here.
            if (account.Title == null)
                account.Title = "Mr";


            var titleTypeEnums = Enum.GetValues(typeof(TitleTypeEnum));

            var selectListItems = new List<SelectListItem>();

            foreach (var i in titleTypeEnums)
            {
                var val = Enum.GetName(typeof(TitleTypeEnum), i);
                var newItem = new SelectListItem
                {
                    Text = val,
                    Value = val,
                    Selected = val == account.Title
                };
                selectListItems.Add(newItem);
            }

            var selectList = new SelectList(selectListItems, "Value", "Text", account.Title);

            ViewData["TitleEnums"] = selectList;
        }

        private bool ResetPasswordRequestSendEmail(ActivationEmailModel emailModel, string resetCode)
        {
            emailModel.ResetPasswordCode = resetCode;
            AccountService.SendActivationPartialToEmail(emailModel, "Password Reset – My EventBook Account", "EmailTemplates/ResetPasswordEmail " + emailModel.ResetPasswordLink);
            return true;
        }

    }
}
