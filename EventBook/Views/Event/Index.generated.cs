﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ID7Digital.EventBook.UI.Views.Event
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    
    #line 8 "..\..\Views\Event\Index.cshtml"
    using System.Web;
    
    #line default
    #line hidden
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using System.Web.Mvc.Html;
    using System.Web.Optimization;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.WebPages;
    
    #line 9 "..\..\Views\Event\Index.cshtml"
    using Microsoft.AspNet.Identity;
    
    #line default
    #line hidden
    
    #line 10 "..\..\Views\Event\Index.cshtml"
    using Microsoft.Owin.Security;
    
    #line default
    #line hidden
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "2.0.0.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/Views/Event/Index.cshtml")]
    public partial class Index : System.Web.Mvc.WebViewPage<dynamic>
    {
        public Index()
        {
        }
        public override void Execute()
        {
WriteLiteral("\r\n");

WriteLiteral("\r\n\r\n");

            
            #line 13 "..\..\Views\Event\Index.cshtml"
 if (Request.IsAuthenticated)
{

            
            #line default
            #line hidden
WriteLiteral("    <h3>Welcome ");

            
            #line 15 "..\..\Views\Event\Index.cshtml"
           Write(User.Identity.GetUserName());

            
            #line default
            #line hidden
WriteLiteral(" </h3>\r\n");

            
            #line 16 "..\..\Views\Event\Index.cshtml"

    // Angular directives
    // Main name of the ng module, and controller to manage the services, ng view for where view directed in spa.

            
            #line default
            #line hidden
WriteLiteral("    <div");

WriteLiteral(" data-ng-app=\"main\"");

WriteLiteral(" data-ng-controller=\"RootController\"");

WriteLiteral(">\r\n        <ul>\r\n            <li");

WriteLiteral(" data-ng-class=\"{active : activeViewPath===\'/demo\'}\"");

WriteLiteral("><a");

WriteLiteral(" href=\"#/demo\"");

WriteLiteral(">Demo</a></li>\r\n            <li");

WriteLiteral(" data-ng-class=\"{active : activeViewPath===\'/account\'}\"");

WriteLiteral("><a");

WriteLiteral(" href=\"#/account\"");

WriteLiteral(">Account Settings</a></li>\r\n        </ul>\r\n        <div");

WriteLiteral(" data-ng-view=\"\"");

WriteLiteral("></div>\r\n    </div>\r\n");

            
            #line 26 "..\..\Views\Event\Index.cshtml"

}
else
{

            
            #line default
            #line hidden
WriteLiteral("    <h4>You are not logged in, Please use a service to log in.</h4>\r\n");

WriteLiteral("    <hr />\r\n");

            
            #line 32 "..\..\Views\Event\Index.cshtml"

    //get list of configured external authentication middleware
    var loginProviders = Context.GetOwinContext().Authentication.GetExternalAuthenticationTypes();
    if (loginProviders.Count() == 0)
    {

            
            #line default
            #line hidden
WriteLiteral("        <div>\r\n            <p>There are no external authentication services confi" +
"gured</p>\r\n        </div>\r\n");

            
            #line 40 "..\..\Views\Event\Index.cshtml"
    }
    else
    {
        using (Html.BeginForm("ExternalLogin", "Event"))
        {
            
            
            #line default
            #line hidden
            
            #line 45 "..\..\Views\Event\Index.cshtml"
       Write(Html.AntiForgeryToken());

            
            #line default
            #line hidden
            
            #line 45 "..\..\Views\Event\Index.cshtml"
                                    

            
            #line default
            #line hidden
WriteLiteral("            <div");

WriteLiteral(" id=\"socialLoginList\"");

WriteLiteral(">\r\n                <p>\r\n");

            
            #line 48 "..\..\Views\Event\Index.cshtml"
                    
            
            #line default
            #line hidden
            
            #line 48 "..\..\Views\Event\Index.cshtml"
                     foreach (AuthenticationDescription p in loginProviders)
                    {

            
            #line default
            #line hidden
WriteLiteral("                        <button");

WriteLiteral(" type=\"submit\"");

WriteLiteral(" class=\"btn btn-default\"");

WriteAttribute("id", Tuple.Create(" id=\"", 1617), Tuple.Create("\"", 1643)
            
            #line 50 "..\..\Views\Event\Index.cshtml"
, Tuple.Create(Tuple.Create("", 1622), Tuple.Create<System.Object, System.Int32>(p.AuthenticationType
            
            #line default
            #line hidden
, 1622), false)
);

WriteLiteral("\r\n                                name=\"provider\"");

WriteAttribute("value", Tuple.Create(" value=\"", 1693), Tuple.Create("\"", 1722)
            
            #line 51 "..\..\Views\Event\Index.cshtml"
, Tuple.Create(Tuple.Create("", 1701), Tuple.Create<System.Object, System.Int32>(p.AuthenticationType
            
            #line default
            #line hidden
, 1701), false)
);

WriteAttribute("title", Tuple.Create("\r\n                                title=\"", 1723), Tuple.Create("\"", 1800)
, Tuple.Create(Tuple.Create("", 1764), Tuple.Create("Log", 1764), true)
, Tuple.Create(Tuple.Create(" ", 1767), Tuple.Create("in", 1768), true)
, Tuple.Create(Tuple.Create(" ", 1770), Tuple.Create("using", 1771), true)
, Tuple.Create(Tuple.Create(" ", 1776), Tuple.Create("your", 1777), true)
            
            #line 52 "..\..\Views\Event\Index.cshtml"
, Tuple.Create(Tuple.Create(" ", 1781), Tuple.Create<System.Object, System.Int32>(p.Caption
            
            #line default
            #line hidden
, 1782), false)
, Tuple.Create(Tuple.Create(" ", 1792), Tuple.Create("account", 1793), true)
);

WriteLiteral(">\r\n");

WriteLiteral("                            ");

            
            #line 53 "..\..\Views\Event\Index.cshtml"
                       Write(p.AuthenticationType);

            
            #line default
            #line hidden
WriteLiteral("\r\n                            </button>\r\n");

            
            #line 55 "..\..\Views\Event\Index.cshtml"
                    }

            
            #line default
            #line hidden
WriteLiteral("                </p>\r\n            </div>\r\n");

            
            #line 58 "..\..\Views\Event\Index.cshtml"
        }
    }
}
            
            #line default
            #line hidden
        }
    }
}
#pragma warning restore 1591
