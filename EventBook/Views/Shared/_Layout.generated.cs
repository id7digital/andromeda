﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ID7Digital.EventBook.UI.Views.Shared
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using System.Web.Mvc.Html;
    using System.Web.Optimization;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.WebPages;
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "2.0.0.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/Views/Shared/_Layout.cshtml")]
    public partial class Layout : System.Web.Mvc.WebViewPage<dynamic>
    {
        public Layout()
        {
        }
        public override void Execute()
        {
WriteLiteral("<!DOCTYPE html>\r\n<html");

WriteLiteral(" lang=\"en\"");

WriteLiteral(">\r\n<head>\r\n    <meta");

WriteLiteral(" charset=\"utf-8\"");

WriteLiteral(" />\r\n    <title>");

            
            #line 5 "..\..\Views\Shared\_Layout.cshtml"
      Write(ViewBag.Title);

            
            #line default
            #line hidden
WriteLiteral(" - EventBook</title>\r\n    <link");

WriteAttribute("href", Tuple.Create(" href=\"", 129), Tuple.Create("\"", 149)
, Tuple.Create(Tuple.Create("", 136), Tuple.Create<System.Object, System.Int32>(Href("~/favicon.ico")
, 136), false)
);

WriteLiteral(" rel=\"shortcut icon\"");

WriteLiteral(" type=\"image/x-icon\"");

WriteLiteral(" />\r\n    <meta");

WriteLiteral(" name=\"viewport\"");

WriteLiteral(" content=\"width=device-width\"");

WriteLiteral(" />\r\n");

WriteLiteral("    ");

            
            #line 8 "..\..\Views\Shared\_Layout.cshtml"
Write(Styles.Render("~/Content/css"));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

WriteLiteral("    ");

            
            #line 9 "..\..\Views\Shared\_Layout.cshtml"
Write(Scripts.Render("~/bundles/modernizr"));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

WriteLiteral("    ");

            
            #line 10 "..\..\Views\Shared\_Layout.cshtml"
Write(Scripts.Render("~/bundles/jquery"));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

WriteLiteral("    ");

            
            #line 11 "..\..\Views\Shared\_Layout.cshtml"
Write(Scripts.Render("~/bundles/js/noty"));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

WriteLiteral("    ");

            
            #line 12 "..\..\Views\Shared\_Layout.cshtml"
Write(Scripts.Render("~/bundles/jqueryval"));

            
            #line default
            #line hidden
WriteLiteral("\r\n\r\n\r\n</head>\r\n<body>\r\n\r\n    <header>\r\n        <div");

WriteLiteral(" class=\"content-wrapper\"");

WriteLiteral(">\r\n            <div");

WriteLiteral(" class=\"float-left\"");

WriteLiteral(">\r\n");

            
            #line 21 "..\..\Views\Shared\_Layout.cshtml"
                
            
            #line default
            #line hidden
            
            #line 21 "..\..\Views\Shared\_Layout.cshtml"
                 if (Request.IsAuthenticated)
                {

            
            #line default
            #line hidden
WriteLiteral("                    <p");

WriteLiteral(" class=\"site-title\"");

WriteLiteral("><a");

WriteLiteral(" href=\"/AccountManagement\"");

WriteLiteral(" data-area=\"/AccountManagement/ProfileManagement\"");

WriteLiteral(">Event Book</a></p>\r\n");

            
            #line 24 "..\..\Views\Shared\_Layout.cshtml"
                }
                else
                {

            
            #line default
            #line hidden
WriteLiteral("                    <p");

WriteLiteral(" class=\"site-title\"");

WriteLiteral(">");

            
            #line 27 "..\..\Views\Shared\_Layout.cshtml"
                                     Write(Html.ActionLink("Event Book", "Index", "Event"));

            
            #line default
            #line hidden
WriteLiteral("</p>\r\n");

            
            #line 28 "..\..\Views\Shared\_Layout.cshtml"
                }

            
            #line default
            #line hidden
WriteLiteral("            </div>\r\n            <div");

WriteLiteral(" class=\"float-right\"");

WriteLiteral(">\r\n                <nav>\r\n                    <ul");

WriteLiteral(" id=\"menu\"");

WriteLiteral(">\r\n");

            
            #line 33 "..\..\Views\Shared\_Layout.cshtml"
                        
            
            #line default
            #line hidden
            
            #line 33 "..\..\Views\Shared\_Layout.cshtml"
                         if (!Request.IsAuthenticated)
                        {

            
            #line default
            #line hidden
WriteLiteral("                            <li><a");

WriteLiteral(" href=\"/Event/SignUp\"");

WriteLiteral(" data-area=\"/Event/SignUp\"");

WriteLiteral(">Register</a></li>\r\n");

            
            #line 36 "..\..\Views\Shared\_Layout.cshtml"

                        }

            
            #line default
            #line hidden
WriteLiteral("\r\n\r\n");

            
            #line 40 "..\..\Views\Shared\_Layout.cshtml"
                        
            
            #line default
            #line hidden
            
            #line 40 "..\..\Views\Shared\_Layout.cshtml"
                         if (Request.IsAuthenticated)
                        {

            
            #line default
            #line hidden
WriteLiteral("                            <li><input");

WriteLiteral(" type=\"text\"");

WriteLiteral(" data-area=\"/Friend/SearchUser\"");

WriteLiteral(" class=\"search-user-base\"");

WriteLiteral(" value=\"search user or event\"");

WriteLiteral(" /></li>\r\n");

WriteLiteral("                            <li><a");

WriteLiteral(" title=\"Contact Info\"");

WriteLiteral(" href=\"/AccountManagement\"");

WriteLiteral(" data-area=\"/AccountManagement/AdditionalContactInformation\"");

WriteLiteral(">Account Settings</a></li>\r\n");

WriteLiteral("                            <li><a");

WriteLiteral(" title=\"Avatar\"");

WriteLiteral(" href=\"/AccountManagement\"");

WriteLiteral(" data-area=\"/AccountManagement/ProfilePicture\"");

WriteLiteral(">Avatar</a></li>\r\n");

WriteLiteral("                            <li><a");

WriteLiteral(" href=\"/AccountManagement/SignOut\"");

WriteLiteral(" data-area=\"/AccountManagement/SignOut\"");

WriteLiteral(" data-redirect=\"true\"");

WriteLiteral(">Log Off</a></li>\r\n");

            
            #line 46 "..\..\Views\Shared\_Layout.cshtml"

                        }

            
            #line default
            #line hidden
WriteLiteral("                    </ul>\r\n                </nav>\r\n            </div>\r\n        </" +
"div>\r\n    </header>\r\n    <div");

WriteLiteral(" id=\"body\"");

WriteLiteral(">\r\n");

WriteLiteral("        ");

            
            #line 54 "..\..\Views\Shared\_Layout.cshtml"
   Write(RenderSection("featured", required: false));

            
            #line default
            #line hidden
WriteLiteral("\r\n        <section");

WriteLiteral(" class=\"content-wrapper main-content clear-fix\"");

WriteLiteral(">\r\n");

WriteLiteral("            ");

            
            #line 56 "..\..\Views\Shared\_Layout.cshtml"
       Write(RenderBody());

            
            #line default
            #line hidden
WriteLiteral("\r\n        </section>\r\n    </div>\r\n    <footer>\r\n        <div");

WriteLiteral(" class=\"content-wrapper\"");

WriteLiteral(">\r\n            <div");

WriteLiteral(" class=\"float-left\"");

WriteLiteral(">\r\n                <p>&copy; ");

            
            #line 62 "..\..\Views\Shared\_Layout.cshtml"
                     Write(DateTime.Now.Year);

            
            #line default
            #line hidden
WriteLiteral(" - EventBook</p>\r\n            </div>\r\n        </div>\r\n    </footer>\r\n\r\n\r\n");

WriteLiteral("    ");

            
            #line 68 "..\..\Views\Shared\_Layout.cshtml"
Write(RenderSection("scripts", required: false));

            
            #line default
            #line hidden
WriteLiteral("\r\n</body>\r\n</html>\r\n");

        }
    }
}
#pragma warning restore 1591
