﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ID7Digital.EventBook.UI.Views.Friend
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using System.Web.Mvc.Html;
    using System.Web.Optimization;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.WebPages;
    
    #line 1 "..\..\Views\Friend\Friends.cshtml"
    using ID7Digital.EventBook.Domain.Utilities;
    
    #line default
    #line hidden
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "2.0.0.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/Views/Friend/Friends.cshtml")]
    public partial class Friends : System.Web.Mvc.WebViewPage<IEnumerable<ID7Digital.EventBook.Services.ViewModels.FriendViewModel>>
    {
        public Friends()
        {
        }
        public override void Execute()
        {
            
            #line 4 "..\..\Views\Friend\Friends.cshtml"
  
    ViewBag.Title = "Friends";

            
            #line default
            #line hidden
WriteLiteral("\r\n\r\n<div");

WriteLiteral(" class=\"friends-dialogue\"");

WriteLiteral(">\r\n    <table>\r\n        <tr>\r\n            <th>\r\n");

WriteLiteral("                ");

            
            #line 12 "..\..\Views\Friend\Friends.cshtml"
           Write(Html.DisplayNameFor(model => model.UserName));

            
            #line default
            #line hidden
WriteLiteral("\r\n            </th>\r\n            <th>\r\n");

WriteLiteral("                ");

            
            #line 15 "..\..\Views\Friend\Friends.cshtml"
           Write(Html.DisplayNameFor(model => model.FirstName));

            
            #line default
            #line hidden
WriteLiteral("\r\n            </th>\r\n            <th></th>\r\n        </tr>\r\n\r\n");

            
            #line 20 "..\..\Views\Friend\Friends.cshtml"
        
            
            #line default
            #line hidden
            
            #line 20 "..\..\Views\Friend\Friends.cshtml"
         foreach (var item in Model)
        {

            
            #line default
            #line hidden
WriteLiteral("            <tr>\r\n                <td>\r\n");

WriteLiteral("                    ");

            
            #line 24 "..\..\Views\Friend\Friends.cshtml"
               Write(Html.DisplayFor(modelItem => item.UserName));

            
            #line default
            #line hidden
WriteLiteral("\r\n                </td>\r\n                <td>\r\n");

WriteLiteral("                    ");

            
            #line 27 "..\..\Views\Friend\Friends.cshtml"
               Write(Html.DisplayFor(modelItem => item.FirstName));

            
            #line default
            #line hidden
WriteLiteral("\r\n                </td>\r\n                <td>\r\n                    <input");

WriteLiteral(" type=\"button\"");

WriteLiteral(" value=\"Add As Friend\"");

WriteLiteral(" id=\"AddFriendButton\"");

WriteLiteral(" data-friend=\"");

            
            #line 30 "..\..\Views\Friend\Friends.cshtml"
                                                                                            Write(item.UserName);

            
            #line default
            #line hidden
WriteLiteral("\"");

WriteLiteral(" data-url=\"/Friend/AddFriend\"");

WriteLiteral(" data-user=\"");

            
            #line 30 "..\..\Views\Friend\Friends.cshtml"
                                                                                                                                                    Write(CurrentUser.UserInformation.AccountId);

            
            #line default
            #line hidden
WriteLiteral("\"");

WriteLiteral(" />\r\n                </td>\r\n\r\n            </tr>\r\n");

            
            #line 34 "..\..\Views\Friend\Friends.cshtml"
        }

            
            #line default
            #line hidden
WriteLiteral("\r\n    </table>\r\n\r\n</div>\r\n");

        }
    }
}
#pragma warning restore 1591
