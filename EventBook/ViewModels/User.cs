﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventBook.ViewModels
{

    enum TitleEnum
    {
        Mr = 1,
        Mrs, 
        Miss,
        Doctor
    }

    public class User
    {

        public string Title { get; set; }
        public string FirstName {get; set;}
        public string LastName { get; set; }


    }
}