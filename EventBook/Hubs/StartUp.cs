﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using System;
using System.Configuration;
using System.Threading.Tasks;

[assembly: OwinStartup(typeof(ID7Digital.EventBook.UI.Hubs.StartUp))]
namespace ID7Digital.EventBook.UI.Hubs
{
    public class StartUp
    {     
            public void Configuration(IAppBuilder app)
            {
                const string XmlSchemaString = "http://www.w3.org/2001/XMLSchema#string";

                // Any connection or hub wire up and configuration should go here
                app.MapSignalR();
                // Configure the db context and user manager to use a single instance per request
    

                app.UseCookieAuthentication(new CookieAuthenticationOptions
                {
                    AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie


                });
                // Use a cookie to temporarily store information about a user logging in with a third party login provider
                app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);


                // Facebook : Create New App
                // https://developers.facebook.com/apps
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings.Get("FacebookAppId")))
                {
                    var facebookOptions = new Microsoft.Owin.Security.Facebook.FacebookAuthenticationOptions
                    {
                        AppId = ConfigurationManager.AppSettings.Get("FacebookAppId"),
                        AppSecret = ConfigurationManager.AppSettings.Get("FacebookAppSecret"),
                        Provider = new Microsoft.Owin.Security.Facebook.FacebookAuthenticationProvider()
                        {
                            OnAuthenticated = (context) =>
                            {
                                context.Identity.AddClaim(new System.Security.Claims.Claim("urn:facebook:access_token", context.AccessToken, XmlSchemaString, "Facebook"));
                                context.Identity.AddClaim(new System.Security.Claims.Claim("urn:facebook:email", context.Email, XmlSchemaString, "Facebook"));
                                return Task.FromResult(0);
                            }
                        },
                        SignInAsAuthenticationType = DefaultAuthenticationTypes.ExternalCookie
                     
                    };

                    facebookOptions.Scope.Add("email");
                    app.UseFacebookAuthentication(facebookOptions);
                }
                
            }        
    }
}