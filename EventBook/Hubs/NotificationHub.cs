﻿using System.Collections.Generic;
using Microsoft.AspNet.SignalR;

namespace ID7Digital.EventBook.UI.Hubs
{
    public class NotificationHub : Hub
    {
        public static Dictionary<string, dynamic> connectedClients = new Dictionary<string, dynamic>();

        public void Send(string name, string message)
        {
            Clients.All.addNewMessageToPage(name, message);
        }

        public void ClientLoggedIn(string userName)
        {
            lock (connectedClients)
            {
                if (connectedClients.ContainsKey(userName))
                {
                    connectedClients[userName] = Clients.Caller;
                }
                else
                {
                    connectedClients.Add(userName, Clients.Caller);
                }
            }

            Clients.Caller.addMessage("test");
        }

        public void AddNotification(string notificationMessage, string toUser)
        {
            lock (connectedClients)
            {
                if (connectedClients.ContainsKey(toUser))
                {
                    dynamic client = connectedClients[toUser];

                    client.addMessage(notificationMessage);
                }
            }
        }
    }
}