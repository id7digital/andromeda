﻿
using ID7Digital.EventBook.Services.Abstract;
using ID7Digital.EventBook.Services.Concrete;
using Ninject;
using Ninject.Modules;
using Ninject.Web.Common;

namespace ID7Digital.EventBook.UI.Infastructure
{
    public static class NinjectIocContainer
    {
        /// <summary>
        /// Ninject Kernel to support DI
        /// </summary>
        public static IKernel Kernel { get; set; }
    }

    public class NinjectControllerFactory 
    {
        private IKernel kernel = new StandardKernel(new EventBookModule());

        public NinjectControllerFactory() {

            kernel = new StandardKernel(new EventBookModule());
            NinjectIocContainer.Kernel = kernel;
        }

        public class EventBookModule : NinjectModule
        {
            public override void Load()
            {
                Bind<IAccountService>().ToMethod(context => new AccountService()).InRequestScope();
                Bind<IUserService>().ToMethod(context => new UserService()).InRequestScope();
                Bind<IFriendService>().ToMethod(context => new FriendService()).InRequestScope();
                Bind<IStatusUpdateService>().ToMethod(context => new StatusUpdateService()).InRequestScope();
                Bind<IFriendInviteService>().ToMethod(context => new FriendInviteService()).InRequestScope();                
            }
        }
    }
}