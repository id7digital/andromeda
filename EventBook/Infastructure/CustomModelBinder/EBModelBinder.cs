﻿using Ninject;
using System.Web.Mvc;

namespace ID7Digital.EventBook.UI.Infastructure.CustomModelBinder
{
    public class EBModelBinder : DefaultModelBinder
    {
        private IKernel Kernel;

        public EBModelBinder()
        {
            Kernel = NinjectIocContainer.Kernel;
        }

        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (Kernel == null)
                Kernel = NinjectIocContainer.Kernel ??
                         new StandardKernel(new NinjectControllerFactory.EventBookModule());

            if (bindingContext.ModelType.Namespace != null)
            {
                var  modelTypeNamespace = bindingContext.ModelType.Namespace.ToUpperInvariant(); //store for comparison, .NET is optimised for ToUpper
                if (bindingContext.ModelType.IsInterface)
                {
                    var me = Kernel.Get(bindingContext.ModelType);
                    return me;
                }
            }

            return base.BindModel(controllerContext, bindingContext);
        }
    }
}