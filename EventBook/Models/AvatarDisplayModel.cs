﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ID7Digital.EventBook.UI.Models
{
    public class AvatarDisplayModel
    {
        public int UserId { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
    }
}