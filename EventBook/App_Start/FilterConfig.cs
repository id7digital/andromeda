﻿using System.Web;
using System.Web.Mvc;

namespace ID7Digital.EventBook.UI
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}