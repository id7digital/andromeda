﻿using System.IO;
using System.Web;
using System.Web.Mvc;

namespace ID7Digital.EventBook.UI.Shared
{
    public class EBBaseController : Controller
    {
        /// <summary>
        /// Render a view to string
        /// </summary>
        /// <param name="viewName"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public string PartialViewToString(string viewName, object model, HttpContext httpContext = null)
        {
            ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                var context = ControllerContext;

                var viewResult = ViewEngines.Engines.FindPartialView(context, viewName);
                var viewContext = new ViewContext(context, viewResult.View, ViewData, TempData, sw);

                //render the string to the stringwriter
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(context, viewResult.View);

                return sw.GetStringBuilder().ToString();
            }
        }


    }
}
